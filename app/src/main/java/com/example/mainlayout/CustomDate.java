package com.example.mainlayout;

import java.io.Serializable;

/*
 * A custom class to represent dates in a method easier to manipulate for calculation purposes
 */
public class CustomDate implements Serializable {

    /*
     * Represents the first three letters of the name of the day (ie Mon, Tue)
     */
    private String day;
    /*
     * Represents the hour of the day in 24H format
     */
    private long hour;
    /*
      * Represents the minute of the hour
     */
    private long minute;

    /*
     * Creates a new custom date object
     * @param day the first three letters of the name of the day (ie Mon, Tue)
     * @param hour the hour of the day in 24h format
     * @param minute the minute of the hour
     */
    public CustomDate(String day, long hour, long minute){
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }

    /*
    * Returns the day of the custom date
    * @return the day of the custom date
    */
    public String getDay() {
        return day;
    }

    /*
    * Finds the minute of the custom date object
    * @return the minute of the hour
     */
    public long getMinute() {
        return minute;
    }

    /*
    * Finds the hour of the custom date object
    * @return the hour in 24h format
     */
    public long getHour() {
        return hour;
    }
}
