package com.example.mainlayout;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.security.crypto.EncryptedFile;
import androidx.security.crypto.MasterKey;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public class FileSystemHelper {
    private Context c;

    public FileSystemHelper(Context c) {
        this.c = c;
    }

    public boolean write(String data, String filename, String ROOT_FOLDER){
        // Attempt to delete old file
        try {
            File f = new File(ROOT_FOLDER, filename);
            f.delete();
        } catch (Exception e) {
            Log.d("While deleting...", e.getMessage());
        }
        try {
            MasterKey mainKey = new MasterKey.Builder(c)
                    .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                    .build();
            EncryptedFile encryptedFile = new EncryptedFile.Builder(c,
                    new File(ROOT_FOLDER, filename),
                    mainKey,
                    EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
            ).build();


            byte[] fileContent = data
                    .getBytes(StandardCharsets.UTF_8);
            OutputStream outputStream = encryptedFile.openFileOutput();
            outputStream.write(fileContent);
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e){
            Log.d("Write Exception: ", e.getMessage()+": "+Arrays.toString(e.getStackTrace()));
            return false;
        }
    }

    public boolean write(byte[] data, String filename, String ROOT_FOLDER){

        try {
            MasterKey mainKey = new MasterKey.Builder(c)
                    .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                    .build();
            EncryptedFile encryptedFile = new EncryptedFile.Builder(c,
                    new File(ROOT_FOLDER, filename),
                    mainKey,
                    EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
            ).build();
            OutputStream outputStream = encryptedFile.openFileOutput();
            outputStream.write(data);
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (IOException | GeneralSecurityException e){
            Log.e("IO Exception\n",e.getMessage());
        }

        return false;
    }

    public String read(String filename, String ROOT_FOLDER) {
        String userSettings;

        try {
            MasterKey mainKey = new MasterKey.Builder(c)
                    .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                    .build();

            EncryptedFile encryptedFile = new EncryptedFile.Builder(c,
                    new File(ROOT_FOLDER, filename),
                    mainKey,
                    EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
            ).build();

            InputStream inputStream = encryptedFile.openFileInput();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int nextByte = inputStream.read();
            while (nextByte != -1) {
                byteArrayOutputStream.write(nextByte);
                nextByte = inputStream.read();
            }

            byte[] plaintext = byteArrayOutputStream.toByteArray();
            userSettings = new String(plaintext,StandardCharsets.UTF_8);

        } catch (IOException e){
            Log.e("IOException:\n",e.getMessage());
            return null;
        } catch (GeneralSecurityException e){
            Log.e("Security exception:\n",e.getMessage());
            return null;
        }
        Log.d("Read:", userSettings);
        return userSettings;
    }
}
