package com.example.mainlayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * ChequeCheck Main Activity
 * Displays the calendar for user job data tracking, the calculation button, and the button to
 * transition to the user settings activity.
 *
 * Author: Group 25
 * Version: 1.0
 */

public class MainActivity extends AppCompatActivity {

    /*
     * Code used in request for write files
     */
    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    /*
     * Code used in request for read files
     */
    public static final int READFILES_PERMISSION_REQUEST_CODE = 4;
    /*
     * Code used when change settings is called
     */
    public static final int CHANGE_SETTINGS = 100;
    /*
     * Code used when load from google is called
     */
    public static final int LOAD_FROM_BACKUP = 75;

    /*
     * Represents the user settings of the current user
     */
    private UserSettings user;
    /*
     * Represents the list of shifts the user is calculating
     */
    private LinkedList<Shift> shifts;
    /*
     * Represents a historical list of shifts
     */
    private LinkedList<Shift> past_shifts;
    /*
     * The list of shift objects which have been converted to string for display purposes
     */
    private ArrayList<String> sShifts;
    /*
     * The listview which displays the shift objects
     */
    protected ListView listView;
    /*
     * The adapter which configures the listview to display shifts
     */
    protected ArrayAdapter<String> sAdapter;

    @Override
    /*
     * Called on creating on the main activity
     * Initialises the view
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.pin_lock);


        // checks permission for storage
        if (!checkPermissionForExternalStorage() ||
            !checkPermissionForReadfiles()){
            // requests permission if permission not granted
            requestPermissionForExternalStorage();
            requestPermissionForReadfiles();
        }

        // loads settings from storage
        UserDataStore temp = (UserDataStore) getIntent().getSerializableExtra("UserDataStore");
        this.user = temp.us;
        this.shifts = temp.shifts;
        if (this.shifts == null){
            shifts = new LinkedList<Shift>();
        }
        this.past_shifts = temp.pastShifts;

    }

    /*
     * converts the user from the pin enter layout to the main activity layout on correct pin entry
     * and initialises main activity layout
     */
    public void unlock(View v) {
        // finds the pin edit field and extracts the value inside
        TextInputEditText pinField = (TextInputEditText) findViewById(R.id.unlockPin);
        String p = pinField.getText().toString();
        // checks if the pin is correct
        if(p.equals(user.getPin())) {
            // if pin is correct, open main view and change display
            setContentView(R.layout.activity_main);
            // Set onclick listener for calendar view
            CalendarView calendarView=(CalendarView) findViewById(R.id.InputCalendar);
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

                @Override
                public void onSelectedDayChange(CalendarView view, int year, int month,
                                                int dayOfMonth) {
                    // Open shift when user clicks date from calendar
                    openShift(year, month, dayOfMonth);
                }
            });


            // loads the shift list into the listview by calling load shifts
            listView= (ListView)findViewById(R.id.shiftListMain);
            sShifts = new ArrayList<String>();
            loadShifts();
            // removes the tax information boxes from display for now
            TextView tax_view = (TextView) findViewById(R.id.SummaryCardA);
            tax_view.setVisibility(View.GONE);
            TextView mc_view = (TextView) findViewById(R.id.SummaryCardB);
            mc_view.setVisibility(View.GONE);
            TextView hecs_view = (TextView) findViewById(R.id.SummaryCardC);
            hecs_view.setVisibility(View.GONE);
            TextView total_view = (TextView) findViewById(R.id.SummaryCardD);
            total_view.setVisibility(View.GONE);


            // Add long click listener when user wants to delete one of the shifts
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               final int pos, long id) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    // Create a dialog interface asking user if he wants to delete or not
                    builder.setTitle("")
                            .setMessage("Do you wish to remove this shift?")
                            .setPositiveButton("Yes", new
                                    DialogInterface.OnClickListener() {
                                        // user clicks yes
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                            Log.v("Delete shift",shifts.get(pos).toString());
                                            // Delete the shift by removing the shift from the list
                                            shifts.remove(pos);
                                            Toast.makeText(getApplicationContext(), "Shift is removed", Toast.LENGTH_LONG).show();
                                            // Reload the shift view again
                                            loadShifts();
                                        }
                                    })
                            .setNegativeButton("No", new
                                    DialogInterface.OnClickListener() {
                                        // user clicks no
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });

                    builder.create().show();
                    return true;
                }
            });
        } else {
            // tells the user if their pin is wrong
            Toast.makeText(getApplicationContext(), "Incorrect Pin", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * saves settings upon the app closing
     */
    @Override
    protected void onDestroy() {
        // instantiates the helper class to save the settings and shifts locally
        FileSystemHelper fs = new FileSystemHelper(getApplicationContext());
        Gson gson = new Gson();
        // stores settings and shifts in a user data store object
        UserDataStore usd = new UserDataStore(this.user);
        usd.updateShifts(shifts, past_shifts);
        // serialises datastore into json string
        String json = gson.toJson(usd);
        // ensures string is valid
        if(json == null || json.equals("{}")) {
            Log.d("Updating Settings Failed", "json was Null or Empty");
            Log.d("Updating Settings Failed", String.valueOf(this.user==null));
            Log.d("Updating Settings Failed", String.valueOf(usd.us==null));
            Log.d("Updating Settings Failed", String.valueOf(usd.shifts==null));
            return;
        }
        // stores json strings locally through helper class
        boolean written = fs.write(json, "ud.txt", getFilesDir().getAbsolutePath());
        // checks strings were written correctly for error checking
        if(!written) {
            Log.d("Updating Settings Failed", "Write failed!");
            return;
        }
        Log.d("Updating Settings", "Complete");

        super.onDestroy();
    }

    /*
     * saves the user's data when onResume is called
     */
    @Override
    protected void onResume() {
        // instantiates the helper class to save the settings and shifts locally
        FileSystemHelper fs = new FileSystemHelper(getApplicationContext());
        Gson gson = new Gson();
        // stores settings and shifts in a user data store object
        UserDataStore usd = new UserDataStore(this.user);
        usd.updateShifts(shifts, past_shifts);
        // serialises datastore into json string
        String json = gson.toJson(usd);
        // ensures string is valid
        if(json == null || json.equals("{}")) {
            Log.d("Updating Settings Failed", "json was Null or Empty");
            Log.d("Updating Settings Failed", String.valueOf(this.user==null));
            Log.d("Updating Settings Failed", String.valueOf(usd.us==null));
            Log.d("Updating Settings Failed", String.valueOf(usd.shifts==null));
            return;
        }
        // stores json strings locally through helper class
        boolean written = fs.write(json, "ud.txt", getFilesDir().getAbsolutePath());
        // checks strings were written correctly for error checking
        if(!written) {
            Log.d("Updating Settings Failed", "Write failed!");
            return;
        }
        Log.d("Updating Settings", "Complete");

        super.onResume();
    }

    /*
    * saves the users data upon call of onRestart
     */
    @Override
    protected void onRestart() {
        // instantiates the helper class to save the settings and shifts locally
        FileSystemHelper fs = new FileSystemHelper(getApplicationContext());
        Gson gson = new Gson();
        // stores settings and shifts in a user data store object
        UserDataStore usd = new UserDataStore(this.user);
        usd.updateShifts(shifts, past_shifts);
        // serialises datastore into json string
        String json = gson.toJson(usd);
        // ensures string is valid
        if(json == null || json.equals("{}")) {
            Log.d("Updating Settings Failed", "json was Null or Empty");
            Log.d("Updating Settings Failed", String.valueOf(this.user==null));
            Log.d("Updating Settings Failed", String.valueOf(usd.us==null));
            Log.d("Updating Settings Failed", String.valueOf(usd.shifts==null));
            return;
        }
        // stores json strings locally through helper class
        boolean written = fs.write(json, "ud.txt", getFilesDir().getAbsolutePath());
        // checks strings were written correctly for error checking
        if(!written) {
            Log.d("Updating Settings Failed", "Write failed!");
            return;
        }
        Log.d("Updating Settings", "Complete");

        super.onRestart();
    }

    /*
     * opens the shift add activity
     * @param year the year clicked on the calendar
     * @param month the month clicked on the calendar
     * @param dayOfMonth the day clicked on the calendar
     */
    private void openShift(int year, int month, int dayOfMonth) {
        Intent intent = new Intent(this, shiftedCalculator.class);

        // Passing year, month and dayOfMonth from main activity
        // Get these data from calendar view object
        intent.putExtra("year", year);
        intent.putExtra("month",month);
        intent.putExtra("dayOfMonth",dayOfMonth);

        // Start the activity and listen to the result
        startActivityForResult(intent, 1);

    }

    /*
    * Opens the edit user settings activity
     */
    public void goToUserSettings(View v) {
        Intent intentSettings = new Intent(this, UserSettingsActivity.class);
        Log.d("Changing Layout", "Main->Settings");

        // stores the user settings and datastore in the intent
        intentSettings.putExtra("settings", user);
        UserDataStore dataStore = new UserDataStore();
        dataStore.updateShifts(this.shifts, this.past_shifts);
        dataStore.updateUserSettings(this.user);
        intentSettings.putExtra("datastore", dataStore);

        // Start the activity and listen to the result
        this.startActivityForResult(intentSettings, CHANGE_SETTINGS);
    }

    /*
    * checks if the system has permission to read files
    * @return true if permission is granted, false if not
     */
    public boolean checkPermissionForReadfiles(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    /*
     * checks if the system has permission to write files
     * @return true if permission is granted, false if not
     */
    public boolean checkPermissionForExternalStorage(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    /*
     * requests permission to write files
     */
    public void requestPermissionForExternalStorage(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            Toast.makeText(this, "External Storage permission needed. Please allow in App Settings for functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            System.out.print("get storage permission");
        }
    }

    /*
     * requests permission to read files
     */
    public void requestPermissionForReadfiles(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
            Toast.makeText(this, "Read files permission needed. Please allow in App Settings for functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},READFILES_PERMISSION_REQUEST_CODE);
        }
    }

    /*
     * handling return from other activities
     * @param requestCode the code sent from this activity
     * @param resultCode the code sent from the returning activity
     * @param data the intent being sent from the returning activity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // saves user settings on return from edit user settings
        if (requestCode == CHANGE_SETTINGS &&  resultCode == 1000) {
            // instantiates the helper class to save the settings and shifts locally
            FileSystemHelper fs = new FileSystemHelper(getApplicationContext());
            Gson gson = new Gson();
            // stores settings and shifts in a user data store object
            UserSettings ret = (UserSettings) data.getSerializableExtra("settings");
            UserDataStore usd = new UserDataStore(ret);
            usd.updateShifts(shifts, past_shifts);
            // serialises datastore into json string
            String json = gson.toJson(usd);
            // ensures string is valid
            if(json == null || json.equals("{}")) {
                Log.d("Updating Settings Failed", "json was Null or Empty");
                Log.d("Updating Settings Failed", String.valueOf(ret==null));
                Log.d("Updating Settings Failed", String.valueOf(usd.us==null));
                Log.d("Updating Settings Failed", String.valueOf(usd.shifts==null));
                return;
            }
            // writes settings locally using helper class
            boolean written = fs.write(json, "ud.txt", getFilesDir().getAbsolutePath());
            // checks write was successful
            if(!written) {
                Log.d("Updating Settings Failed", "Write failed!");
                return;
            }
            Log.d("Updating Settings", "Complete");
            this.user = ret;
        }
        // returning from add shift
        else if (resultCode == 1111) {

            // adds the shift object being returned from the result
            Shift newShift = (Shift) data.getSerializableExtra("result");
            shifts.add(newShift);
            loadShifts();
            Log.d("Adding shifts", "Shift "+newShift.getDay()+" is added");

        }
        // returning from load from google backup
        else if (resultCode == LOAD_FROM_BACKUP){
            //updates settings based on values from google backup
            UserDataStore dataStore = (UserDataStore) data.getSerializableExtra("datastore");
            this.user = dataStore.getUserSettings();
            this.shifts = dataStore.getShifts();
            loadShifts();
        }


    }

    /*
     * loads the shift objects into the listview
     */
    private void loadShifts(){

            sShifts.clear();
            // iterating over every shift in the list
            for (Shift shift : this.shifts) {

                SimpleDateFormat date = new SimpleDateFormat("dd:MM:yy");
                SimpleDateFormat time = new SimpleDateFormat("HH:mm");
                // extracting data from the shifts and formatting it for display
                String dateOfShift = shift.getDay();
                String startTime = time.format(shift.getStartTime());
                String endTime = time.format(shift.getEndTime());
                String jobName = shift.getJobName();
                // add the string representation of the job to a list of strings
                sShifts.add("Job name: " + jobName + "\nDate: " + dateOfShift +
                        "\n" + startTime + " - " + endTime);

                Log.d("Adding shifts", "Shift "+"Job name: " + jobName + "\nDate: " + dateOfShift +
                        "\n" + startTime + " - " + endTime+" is added to the listview");
            }

            // set the adapter to the list of job strings
            sAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, sShifts);
            // assign the adapter to the listview
            listView.setAdapter(sAdapter);

    }


    /*
     * performs the tax calculations and displays it to the user
     * called on click of the Cheque Check button
     */
    public void calculateClick(View view) {
        // Build and income calculator, give it the user settings and shifts
        IncomeCalculator incomeCalculator = new IncomeCalculator(user, shifts);

        // finds the expected annual income based off user and shifts
        double preTaxIncome = incomeCalculator.findExpectedAnnualIncomeBeforeTax();
        // finds the income generated from the shifts
        double preTaxShiftIncome = incomeCalculator.findIncomeBeforeTax();
        // represents the payschedule, used to divide data for correct display
        long divisor;
        divisor = incomeCalculator.paySchedule;
        Log.e("Divisor: ", String.valueOf(divisor));

        // checks to make sure an error wasn't thrown in the incomeCalculator
        if (preTaxIncome == -1){
            Toast.makeText(this, "Invalid shift", Toast.LENGTH_LONG);
            return;
        }
        // Build a tax calc instance (total pay, isHecs, isMedicare)
        Log.e("Pretax:",String.valueOf(preTaxIncome));
        TaxCalculator taxCalculator = new TaxCalculator((int) preTaxIncome);
        // Get Tax!
        double tax = taxCalculator.getSimpleTax();
        double medicareLevy = taxCalculator.getMedicareLevy();
        double hecs = taxCalculator.getStudentLoanRepayment();

        // Gets the taxation data of annual income to figure out how much was generated by the added
        // shifts
        TaxCalculator prevTaxCalculator = new TaxCalculator((int)user.getAnnualIncome());
        double prevTax = prevTaxCalculator.getSimpleTax();
        double prevMedicareLevy = prevTaxCalculator.getMedicareLevy();
        double prevHecs = prevTaxCalculator.getStudentLoanRepayment();

        // Configures the taxation display box to display the amount of post tax income
        TextView tax_view = (TextView) findViewById(R.id.SummaryCardA);
        tax_view.setVisibility(View.VISIBLE);
        String tax_readout = String.format(getResources().getString(R.string.calculation_tax_card)
                ,"<b>", "</b><p style='text-align: right; font-size:24px'>", ((tax-prevTax)/divisor),"</p>");
        tax_view.setText(HtmlCompat.fromHtml(tax_readout, HtmlCompat.FROM_HTML_MODE_COMPACT));

        // Configures the medicare display box to display the amount of taxation through medicare
        TextView mc_view = (TextView) findViewById(R.id.SummaryCardB);
        mc_view.setVisibility(View.VISIBLE);
        String mc_readout = String.format(getResources().getString(R.string.calculation_medicare_card)
                ,"<b>", "</b><p style='text-align: right; font-size:24px'>", ((medicareLevy-prevMedicareLevy)/divisor),"</p>");
        mc_view.setText(HtmlCompat.fromHtml(mc_readout, HtmlCompat.FROM_HTML_MODE_COMPACT));

        // Configures the hecs display box to display the amount of taxation through hecs
        TextView hecs_view = (TextView) findViewById(R.id.SummaryCardC);
        hecs_view.setVisibility(View.VISIBLE);
        String hecs_readout = String.format(getResources().getString(R.string.calculation_hecs_card)
                ,"<b>", "</b><p style='text-align: right; font-size:24px'>", (hecs-prevHecs)/divisor,"</p>");
        Log.d("HECS" ,hecs_readout);
        hecs_view.setText(HtmlCompat.fromHtml(hecs_readout, HtmlCompat.FROM_HTML_MODE_COMPACT));

        // Configures the income display box to display the amount of pre tax income
        TextView total_view = (TextView) findViewById(R.id.SummaryCardD);
        total_view.setVisibility(View.VISIBLE);
        String total_readout = String.format(getResources().getString(R.string.calculation_total_card)
                ,"<b>", "</b><p style='text-align: right; font-size:24px'>", ((preTaxShiftIncome)),"</p>");
        Spanned s = HtmlCompat.fromHtml(total_readout, HtmlCompat.FROM_HTML_MODE_COMPACT);
        total_view.setText(s);
    }
}
