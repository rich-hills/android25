package com.example.mainlayout;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * User Data Store
 *
 * Wrapper Class to allow for easy encryption and decryption of user data into a single json
 *
 * Author: Group 25
 * Version: 1.0
 */

public class UserDataStore implements Serializable {
    protected UserSettings us;
    protected LinkedList<Shift> shifts;
    protected LinkedList<Shift> pastShifts;

    public UserDataStore(UserSettings us) {
        this.us = us;
    }

    public UserDataStore() {    }

    public LinkedList<Shift> getShifts() {
        return shifts;
    }

    public UserSettings getUserSettings() {
        return us;
    }

    public void updateUserSettings(UserSettings newSettings) {
        us = newSettings;
    }

    public void updateShifts(LinkedList<Shift> newShifts, LinkedList<Shift> newPastShifts) {
        this.shifts = newShifts;
        this.pastShifts = newPastShifts;
    }
}
