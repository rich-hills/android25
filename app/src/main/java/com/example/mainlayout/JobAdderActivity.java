package com.example.mainlayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;


/**
 * Job adder activity
 * Add job's information to the database
 *
 * Author: Group 25
 * Version: 1.0
 */
public class JobAdderActivity extends AppCompatActivity {


    /**
     * Override for OnCreate, construct varibales from user input
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_adder);
    }

    /**
     * Method that create a job input forum
     * @param v
     */
    public void createJobOnClick(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Done")
                .setMessage("Are you finished?")
                .setPositiveButton("Yes", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {

                                TextInputEditText nameField = (TextInputEditText) findViewById(R.id.addJobName);
                                TextInputEditText wageField = (TextInputEditText) findViewById(R.id.addJobWage);
                                String name = "";
                                String wage = "";

                                try {
                                    name = Objects.requireNonNull(nameField.getText()
                                            , "Null name").toString();
                                    wage = Objects.requireNonNull(wageField.getText(), "Null pin").toString();
                                } catch(Exception e) {
                                    Log.d("SignupErr",e.getMessage()+Arrays.toString(e.getStackTrace()));
                                    return;
                                }


                                if (validateName() && validatePayRate()){

                                    ArrayList<Job> jobs = (ArrayList<Job>)getIntent().getSerializableExtra("Jobs");

                                    CheckBox casual = (CheckBox)findViewById(R.id.addJobCasual);
                                    CheckBox primary = (CheckBox)findViewById(R.id.addJobPrimary);
                                    boolean isCasual = casual.isChecked();
                                    boolean isPrimary = primary.isChecked();

                                    double w = Double.valueOf(wage);

                                    Job job = new Job(name,w,isPrimary);
                                    job.setCasual(isCasual);

                                    if (isPrimary){
                                        for (Job j : jobs){
                                            j.setPrimaryJob(false);
                                        }
                                    }
                                    jobs.add(job);
                                    Intent data = new Intent();
                                    Log.e("Job: ", jobs.toString());
                                    data.putExtra("Jobs", jobs);
                                    setResult(JobEditorActivity.ADD_JOB_CODE,data);
                                    finish();
                                }
                            }
                        })
                .setNegativeButton("No", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // User cancelled the dialog
                                // Nothing happens
                            }
                        });

        builder.create().show();

    }

    /**
     * Method that cancel the adding job activity
     * @param v
     */
    public void cancelAddJobActivity(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Cancel")
                .setMessage("Do you wish to cancel?")
                .setPositiveButton("Yes", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        })
                .setNegativeButton("No", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // User cancelled the dialog
                                // Nothing happens
                            }
                        });

        builder.create().show();

    }

    private boolean validateName() {
        TextInputEditText nameField = (TextInputEditText) findViewById(R.id.addJobName);
        String name;
        try {
            name = Objects.requireNonNull(nameField.getText(),
                    "Null Name").toString();
        } catch (Exception e) {
            Log.d("validateName",e.getMessage()+ Arrays.toString(e.getStackTrace()));
            return false;
        }
        if(name.length() < 1) {
            Toast.makeText(this,"Make sure to enter a name",Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private boolean validatePayRate() {
        TextInputEditText basePayField = (TextInputEditText) findViewById(R.id.addJobWage);
        String basePay;
        try {
            basePay = Objects.requireNonNull(basePayField.getText(),
                    "Null Pay Rate").toString();
        } catch(Exception e) {
            Log.d("validatePayRate",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return false;
        }
        try {
            Double.parseDouble(basePay);
        } catch (Exception e) {
            Toast.makeText(this,"Make sure to enter a wage",Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }


}
