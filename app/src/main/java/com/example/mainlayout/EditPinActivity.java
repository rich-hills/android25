package com.example.mainlayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;
import java.util.Objects;

/**
 * Edit Pin Activity
 * Activity to handle users editing their pin.
 *
 * Author: Group 25
 * Version: 1.0
 */

public class EditPinActivity extends AppCompatActivity {
    String oldPin; // Stored the "old" pin for verification purposes

    /**
     * Override for OnCreate, populated the oldPin member from Intent extras
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_edit);

        // Extract User Settings
        this.oldPin = (String) getIntent().getSerializableExtra("oldPin");

        // Setup Listeners
        TextInputEditText newPinField = (TextInputEditText) findViewById(R.id.editPinNew);
        newPinField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePin(false);
            }
        });

        TextInputEditText confPinField = (TextInputEditText) findViewById(R.id.editPinConf);
        confPinField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePin(true);
            }
        });

        TextInputEditText oldPinField = (TextInputEditText) findViewById(R.id.editPinOld);
        oldPinField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validateOldPin();
            }
        });
    }

    /**
     * Method to validate the pin, ensuring it meets the standards (is 6 long), or matches the
     * entry field
     * @param conf - are we looking at the pin or confirm pin fields?
     * @return
     */
    public boolean validatePin(boolean conf) {
        TextView err = (TextView) findViewById(R.id.editPinErr);
        err.setText("");
        String pin;
        String pinConf;
        if(conf) {
            TextInputEditText pinField = (TextInputEditText) findViewById(R.id.editPinNew);
            TextInputEditText pinConfField = (TextInputEditText) findViewById(R.id.editPinConf);
            try {
                pin = Objects.requireNonNull(pinField.getText(), "Null Pin").toString();

                pinConf = Objects.requireNonNull(pinConfField.getText(),
                        "Null PinConf").toString();
            } catch (Exception e) {
                Log.d("validatePin",e.getMessage()+ Arrays.toString(e.getStackTrace()));
                return false;
            }
            if(!pin.equals(pinConf)) {
                err.setText(R.string.pin_conf_error);
                return false;
            }
        } else {
            TextInputEditText pinField = (TextInputEditText) findViewById(R.id.editPinNew);
            try {
                pin = Objects.requireNonNull(pinField.getText(), "Null pin").toString();
            } catch (Exception e) {
                Log.d("validatePin",e.getMessage()+Arrays.toString(e.getStackTrace()));
                return false;
            }
            if(pin.length() > 6) {
                err.setText(R.string.pin_length_error_a);
                return false;
            } else if (pin.length() < 6) {
                err.setText(R.string.pin_length_error_b);
                return false;
            }
        }
        return true;
    }

    /**
     * Ensures the user entered old pin matches that stored from the settings
     * @return
     */
    public boolean validateOldPin() {
        TextView err = (TextView) findViewById(R.id.editPinErr);
        err.setText("");
        TextInputEditText pinField = (TextInputEditText) findViewById(R.id.editPinOld);
        String pin;
        try {
            pin = Objects.requireNonNull(pinField.getText(), "Null pin").toString();
        } catch (Exception e) {
            Log.d("validatePin",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return false;
        }
        if(oldPin.equals(pin)) {
            return true;
        } else {
            err.setText(R.string.pin_old_err);
            return false;
        }
    }

    /**
     * Validate the pin, and return intent result
     * @param v
     */
    public void save(View v){
        if (validatePin(true) && validatePin(false) && validateOldPin()) {
            Log.d("PIN", "New Pin Processing");
            TextInputEditText pinField = (TextInputEditText) findViewById(R.id.editPinNew);
            String pin;
            try {
                pin = Objects.requireNonNull(pinField.getText(), "Null pin").toString();

                Intent save = new Intent();
                save.putExtra("newPin", pin);
                setResult(1050, save);
                finish();
            } catch (Exception e) {
                Log.d("validatePin",e.getMessage()+Arrays.toString(e.getStackTrace()));
            }
        } else {
            Log.d("TEST", "Could Validate Pin");
        }
    }

    public void cancel(View view) {
        finish();
    }
}
