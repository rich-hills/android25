package com.example.mainlayout;

import java.io.Serializable;

/**
 * Penalty Object
 *
 * Stores penalty rate information
 *
 * Author: Group 25
 * Version: 1.0
 */
public class Penalty implements Serializable {

    private String day;
    private CustomDate start_time;
    private CustomDate end_time;
    private double penalty;

    /**
     * Constructor
     * @param day
     * @param start_time
     * @param end_time
     * @param penalty
     */
    public Penalty(String day, CustomDate start_time, CustomDate end_time, double penalty){
        this.day = day;
        this.start_time = start_time;
        this.end_time = end_time;
        this.penalty = penalty;
    }

    public String getDay(){
        return this.day;
    }
    public CustomDate getStartTime(){
        return this.start_time;
    }
    public CustomDate getEndTime(){
        return this.end_time;
    }
    public double getRate(){
        return this.penalty;
    }
}
