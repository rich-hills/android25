package com.example.mainlayout;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;

/**
 * StartUp Activity
 * Activity to handle initial file permissions granting, as well as direct users to the pin activity
 * or signup activity. Also facilitates the decryption of user data stored on the phone.
 *
 * Author: Group 25
 * Version: 1.0
 */

public class StartUp extends AppCompatActivity {

    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    public static final int READFILES_PERMISSION_REQUEST_CODE = 4;

    /**
     * Create class instance
     * Checks if stored user data exists, and is readable/not corrupted, then passes it to the main
     * activity. If no data is present, it redirects to the signup activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check file perms
        if (!checkPermissionForExternalStorage() ||
                !checkPermissionForReadfiles()){
            requestPermissionForExternalStorage();
            requestPermissionForReadfiles();
        }

        // Attempt to read IV/UserData
        FileSystemHelper fs = new FileSystemHelper(getApplicationContext());
        String ud = fs.read("ud.txt", getFilesDir().getAbsolutePath());

        if (ud == null || ud.equals("{}")) {
            // No data, call signup
            Intent gotoSignup = new Intent(this, UserSignupActivity.class);
            Log.d("Changing layout", "Startup -> Signup");
            this.startActivity(gotoSignup);
        } else {
            // Decrypt and load files
            Gson gson = new Gson();
            UserDataStore userDataStore = gson.fromJson(ud, UserDataStore.class);
            if(userDataStore == null) {
                Intent gotoSignup = new Intent(this, UserSignupActivity.class);
                Log.d("Changing layout", "Startup -> Signup");
                this.startActivity(gotoSignup);
            }
            Intent gotoMain = new Intent(this, MainActivity.class);
            Log.d("Changing Layout", "Startup -> Main");
            gotoMain.putExtra("UserDataStore", userDataStore);
            this.startActivity(gotoMain);
        }

    }

    public boolean checkPermissionForReadfiles(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public boolean checkPermissionForExternalStorage(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public void requestPermissionForExternalStorage(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            Toast.makeText(this, "External Storage permission needed. Please allow in App Settings for functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            System.out.print("get storage permission");
        }
    }

    public void requestPermissionForReadfiles(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
            Toast.makeText(this, "Read files permission needed. Please allow in App Settings for functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},READFILES_PERMISSION_REQUEST_CODE);
        }
    }
}
