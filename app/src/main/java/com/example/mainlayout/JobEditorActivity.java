package com.example.mainlayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;


/**
 * Job editor activity
 * Edit job's information to the database
 *
 * Author: Group 25
 * Version: 1.0
 */
public class JobEditorActivity extends AppCompatActivity {

    protected final static int CANCEL_CODE = 102;
    protected final static int ADD_JOB_CODE = 103;
    protected final static int EDIT_JOB_CODE = 104;
    protected final static int DELETE_JOB_CODE = 104;
    protected ListView listView;
    protected ArrayList<String> sJobs;
    protected ArrayAdapter<String> sAdapter;
    protected ArrayList<Job> jobs;

    /**
     * Override for OnCreate, construct varibales from user input
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_editor);

        listView= (ListView)findViewById(R.id.jobList);

        sJobs = new ArrayList<String>();

        this.jobs = (ArrayList<Job>)getIntent().getSerializableExtra("Jobs");

        setupListViewListener();
        loadJobs();

    }

    /**
     * Method that add the job
     * @param v
     */
    public void addJobOnClick(View v){

        Intent intent = new Intent(this, JobAdderActivity.class);
        ArrayList<Job> jobs = (ArrayList<Job>)getIntent().getSerializableExtra("Jobs");
        intent.putExtra("Jobs", jobs);
        this.startActivityForResult(intent,ADD_JOB_CODE);

    }

    /**
     * Method that cancel the adding job process
     * @param v
     */
    public void cancelJobOnClick(View v){

        Intent intent = new Intent();
        intent.putExtra("Jobs", this.jobs);
        setResult(UserSettingsActivity.EDIT_JOB, intent);
        finish();

    }

    /**
     * Method that listen to result on different opertions
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == ADD_JOB_CODE) {
            this.jobs = (ArrayList<Job>) data.getSerializableExtra("Jobs");
            loadJobs();}
        if (resultCode == EDIT_JOB_CODE) {
            this.jobs = (ArrayList<Job>) data.getSerializableExtra("Jobs");
            loadJobs();
        }
        if (resultCode == CANCEL_CODE) {
            this.jobs = (ArrayList<Job>) data.getSerializableExtra("Jobs");
            loadJobs();
        }
        if (resultCode == DELETE_JOB_CODE){
            this.jobs = (ArrayList<Job>) data.getSerializableExtra("Jobs");
            loadJobs();
        }
    }

    private void loadJobs(){

        sJobs.clear();
        for (Job job : this.jobs){
            String sJob = (job.getName() +
                    "\nWage:" + job.getWage());
            if (job.checkCasual()){
                sJob.concat("\nCasual");
            } else {
                sJob.concat("\nPart Time/Full Time");
            }
            if (job.getJobStatus()){
                sJob.concat("Primary job");
            } else {
                sJob.concat("Secondary job");
            }

            sJobs.add(sJob);
        }

        sAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sJobs);
        listView.setAdapter(sAdapter);

    }

    private void setupListViewListener() {

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int
                    position, long rowId)
            {
                ;
            AlertDialog.Builder builder = new AlertDialog.Builder(JobEditorActivity.this);
            builder.setMessage("Do you want to change the job?")
                    .setPositiveButton("Yes", new
                            DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(JobEditorActivity.this,
                                            IndividualJobEditorActivity.class);
                                    intent.putExtra("Job",jobs.get(position));
                                    intent.putExtra("Jobs",jobs);
                                    JobEditorActivity.this.startActivityForResult(intent, EDIT_JOB_CODE);
                                }
                            })
                    .setNegativeButton("No", new
                            DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });

            builder.create().show();
            return true;

            }
        });

    }

}
