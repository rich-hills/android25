package com.example.mainlayout;

import java.io.Serializable;
import java.util.Date;

/**
 * Shift Object
 * Store a job shift
 *
 * Author: Group 25
 * Version: 1.0
 */
public class Shift implements Serializable {

    private String job_name;
    private String day;
    private Date start;
    private Date end;

    /**
     * Construct
     * @param job_name
     * @param day
     * @param start
     * @param end
     */
    public Shift(String job_name, String day, Date start, Date end){
        this.job_name = job_name;
        this.day = day;
        this.start = start;
        this.end = end;
    }

    public Date getStartTime(){
        return this.start;
    }
    public Date getEndTime(){
        return this.end;
    }
    public String getDay(){
        return this.day;
    }
    public String getJobName() {return this.job_name;}

}
