package com.example.mainlayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/*
 * Activity that handles the editting of a job
 */
public class IndividualJobEditorActivity extends AppCompatActivity {

    /*
     * The job object to be modified
     */
    protected Job job;
    /*
     * The array list that contains the job object, needed for the remove job option
     */
    protected ArrayList<Job> jobs;
    /*
     * The array list that stores the jobs penalties represented as a string for display
     * in a list view
     */
    protected ArrayList<String> sPenalties;
    /*
     * An adapter to transform the list of penalties into the listview
     */
    protected ArrayAdapter<String> sAdapter;
    /*
     * The list view which stores the list of penalties attached to the job
     */
    protected ListView listView;

    /*
     * The code to handle the addition of a new penalty
     */
    protected final static int PENALTY_ADD_CODE = 200;
    /*
     * The code to handle the cancelation of an addition of a new penalty
     */
    protected final static int PENALTY_CANCEL_CODE = 201;

    /*
     * Called upon creation of the activity
     * Extracts the job and the job list from the intent
     * Sets the display boxes to match the data of the job
     * Converts penalties to a string representation to be displayed in a list view
     */
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.individual_job_editor);

        // extract job and jobs from the intent
        this.job = (Job)getIntent().getSerializableExtra("Job");
        this.jobs = (ArrayList<Job>)getIntent().getSerializableExtra("Jobs");
        // find the display fields as declared in the layout file
        TextInputEditText jobNameField = (TextInputEditText)findViewById(R.id.editJobName);
        TextInputEditText jobWageField = (TextInputEditText)findViewById(R.id.editJobWage);
        CheckBox casualField = (CheckBox)findViewById(R.id.editJobCasual);
        CheckBox primaryField = (CheckBox)findViewById(R.id.editJobPrimary);

        // sets the value of the fields to match that of the job
        jobNameField.setText(job.getName());
        jobWageField.setText(String.valueOf(job.getWage()));
        casualField.setChecked(job.checkCasual());
        primaryField.setChecked(job.getJobStatus());

        // loads the penalties into the list view
        listView= (ListView)findViewById(R.id.penaltyList);
        sPenalties = new ArrayList<String>();
        loadPenalties();
        // sets up listener on list view containing penalties
        setupListViewListener();

    }

    /*
     * Launches add penalty activity on click of add penalty button
     */
    public void addPenaltyOnClick(View view) {

        // attaches job to the intent and calls activity
        Intent intent = new Intent(this, PenaltyEditActivity.class);
        intent.putExtra("Job", job);
        this.startActivityForResult(intent, PENALTY_ADD_CODE);

    }

    /*
     * Saves the changes of the current edits without closing the activity
     */
    public void editJobOnClick(View view) {

        // establishing connection to the text fields
        TextInputEditText jobNameField = (TextInputEditText)findViewById(R.id.editJobName);
        TextInputEditText jobWageField = (TextInputEditText)findViewById(R.id.editJobWage);
        String name = "";
        String wage = "";

        try {
            // checks to make sure the text fields are not null values
            name = Objects.requireNonNull(jobNameField.getText()
                    , "Null name").toString();
            wage = Objects.requireNonNull(jobWageField.getText(), "Null pin").toString();
        } catch(Exception e) {
            Log.d("SignupErr",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return;
        }

        // checks to make sure the pay rate and name fields are valid before proceeding
        if (validatePayRate() && validateName()){

            // opens the job list
            ArrayList<Job> jobs = (ArrayList<Job>)getIntent().getSerializableExtra("Jobs");

            // gets the state of the check boxes about whether job is primary and/or casual
            CheckBox casual = (CheckBox)findViewById(R.id.editJobCasual);
            CheckBox primary = (CheckBox)findViewById(R.id.editJobPrimary);
            boolean isCasual = casual.isChecked();
            boolean isPrimary = primary.isChecked();

            // gets the wage of the job
            double w = Double.valueOf(wage);

            // gets the name of the job
            Job job = new Job(name,w,isPrimary);
            job.setCasual(isCasual);

            // if the job is a primary job, set all other jobs to secondary jobs
            if (isPrimary){
                for (Job j : jobs){
                    j.setPrimaryJob(false);
                }
            }
            // remove the old job object and add the editted one
            for (int i = 0; i < jobs.size(); i++){
                if (jobs.get(i).getName().equals(this.job.getName())){
                    jobs.remove(i);
                    break;
                }
            }
            jobs.add(job);

        }

    }

    /*
     * Calls the method which deletes the currently selected job
     */
    public void deleteJobOnClick(View view) {

        // creates a dialogue to confirm the deletion
        AlertDialog.Builder builder = new AlertDialog.Builder(IndividualJobEditorActivity.this);
        builder
                .setMessage("Do you want to delete this job?")
                .setPositiveButton("Yes", new
                        DialogInterface.OnClickListener() {
                            // on clicking the yes response
                            public void onClick(DialogInterface dialogInterface, int i) {

                                // finds the position of the job in the arraylist and removes it
                                int x = 0;
                                for (Job j : jobs){
                                    if (j.getName().equals(job.getName())){
                                        jobs.remove(x);
                                    }
                                    x = x + 1;
                                }
                                // stores the updated job list in the intent and closes the activity
                                Intent intent = new Intent();
                                intent.putExtra("Jobs", jobs);
                                setResult(JobEditorActivity.DELETE_JOB_CODE, intent);
                                finish();
                            }
                        })
                .setNegativeButton("No", new
                        DialogInterface.OnClickListener() {
                            // on no click
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // User cancelled the dialog
                                // Nothing happens
                            }
                        });

        builder.create().show();
    }

    /*
     * closes the activity upon clicking the cancel button
     */
    public void cancelEditJobActivity(View view) {
        Intent data = new Intent();
        data.putExtra("Jobs", jobs);
        setResult(JobEditorActivity.CANCEL_CODE,data);
        finish();
    }

    /*
     * converts the penalty a list to a string and loads it into the listview
     */
    private void loadPenalties(){

        sPenalties.clear();
        // converting each penalty to a string and adding it to the list of penalty strings
        for (Penalty penalty : this.job.getPenalties()){
            String sPenalty = (penalty.getDay() +
                    "\nPenalty:" + "+" + (penalty.getRate()*100) + "%"+
                    "\nStart Time: " + penalty.getStartTime().getHour() + ":" +
                    penalty.getStartTime().getMinute() +
                    "\nEnd Time: " + penalty.getEndTime().getHour() + ":" +
                    penalty.getEndTime().getMinute());
            sPenalties.add(sPenalty);
        }

        // creates the adapter and settings its content to the string penalty list
        sAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, sPenalties);
        // sets the adapter of the listview
        listView.setAdapter(sAdapter);
    }

    /*
     * handling a return from the add penalty activity
     */
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        // updates the penalty list if a penalty was added
        if (resultCode == PENALTY_ADD_CODE){

            // extracts penalty from intent
            Penalty penalty = (Penalty)data.getSerializableExtra("Penalty");
            // adds the penalty to the job
            job.addPenalty(penalty);
            // reloads the penalty list
            loadPenalties();
        }
        // no changes required if a cancellation was done on add penalty
    }

    /*
     * handles a listener on click of the ListView storing penalties to handle removal of penalties
     */
    private void setupListViewListener() {

        /*
         * listener checks for an extended click on a particular field of the list view
         */
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int
                    position, long rowId)
            {
                ;
                // creates a dialog builder to ask if the user wishes to delete selected penalty
                AlertDialog.Builder builder = new AlertDialog.Builder(IndividualJobEditorActivity.this);
                builder
                        .setMessage("Do you want to delete the penalty?")
                        .setPositiveButton("Yes", new
                                DialogInterface.OnClickListener() {
                                    // on yes click
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        // removes the selected penalty
                                        job.getPenalties().remove(position);
                                        // updates the displayed penalties
                                        loadPenalties();
                                    }
                                })
                        .setNegativeButton("No", new
                                DialogInterface.OnClickListener() {
                                    // on no click
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        // User cancelled the dialog
                                        // Nothing happens
                                    }
                                });

                builder.create().show();
                return true;

            }
        });

    }

    /*
     * validates whether the name field is correct
     * @return true if the name field is valid, false if not
     */
    private boolean validateName() {
        // finds the text field containing name
        TextInputEditText nameField = (TextInputEditText) findViewById(R.id.editJobName);
        String name;
        try {
            // checks the field is not null
            name = Objects.requireNonNull(nameField.getText(),
                    "Null Name").toString();
        } catch (Exception e) {
            Log.d("validateName",e.getMessage()+ Arrays.toString(e.getStackTrace()));
            return false;
        }
        // checks that the name is not empty
        if(name.length() < 1) {
            Toast.makeText(this,"Make sure to enter a name",Toast.LENGTH_LONG);
            return false;
        }
        // returns true if all criteria is met
        return true;
    }

    /*
     * validates whether the pay rate is correct
     * @return true if the wage is valid, false if not
     */
    private boolean validatePayRate() {
        // finds text field containing wage
        TextInputEditText basePayField = (TextInputEditText) findViewById(R.id.editJobWage);
        String basePay;
        // checks the field is not null
        try {
            basePay = Objects.requireNonNull(basePayField.getText(),
                    "Null Pay Rate").toString();
        } catch(Exception e) {
            Log.d("validatePayRate",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return false;
        }
        // checks the field is a valid double that can be parsed
        try {
            Double.parseDouble(basePay);
        } catch (Exception e) {
            Toast.makeText(this,"Make sure to enter a wage",Toast.LENGTH_LONG);
            return false;
        }
        // returns true if all criteria is met
        return true;
    }
}
