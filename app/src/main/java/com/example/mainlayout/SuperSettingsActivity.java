package com.example.mainlayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Edit Super Settings Activity
 *
 * Activity to handle the Super Edit Layout
 *
 * Author: Group 25
 * Version: 1.0
 */
public class SuperSettingsActivity extends AppCompatActivity {

    UserSettings settings;

    EditText superRate;

    /**
     * Create
     * Load passed user settings for editing
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.super_settings);
        this.settings = (UserSettings)getIntent().getSerializableExtra("settings");
        this.superRate=findViewById(R.id.super_rate);

        superRate.setText(String.valueOf(settings.getSuper_rate()*100));
    }

    /**
     * Handle return to settings layout
     * @param view
     */
    public void backToSettingPage(View view){
        Intent intent = new Intent(this, UserSettingsActivity.class);
        intent.putExtra("settings", settings);

        setResult(UserSettingsActivity.EDIT_SUPER, intent);
        finish();
    }

    /**
     * Handle save of super
     * @param view
     */
    public void saveValue(View view){
          double rate=Double.valueOf(superRate.getText().toString());
          if (rate>100){
              Toast toast=Toast.makeText(this,"Input rate can not exceed 100!",
                      Toast.LENGTH_LONG);
              toast.show();
          }else{
              this.settings.setSuperRate(rate/100);
              Toast.makeText(this,"Saved successfully!",
                      Toast.LENGTH_LONG).show();
              backToSettingPage(view);
          }

    }
}
