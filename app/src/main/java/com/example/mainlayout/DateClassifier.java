package com.example.mainlayout;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * A series of static methods to help handle date objects
 */
public class DateClassifier {

    /*
     * Default constructor
     */
    public DateClassifier(){ }


    /*
     * Finds the first three characters of the day of the date object
     * Useful for creating custom date objects
     * @param date the date being tested
     * @return the first three letters of the day
     */
    public static String findDayFromDate(Date date){
        SimpleDateFormat sDay = new SimpleDateFormat("E");
        String out = sDay.format(date);
        return out;
    }

   /*
    * Finds the 24 hour representation of the hours of the current day of the date object
    * Useful for creating custom date objects
    * @param date the date being tested
    * @return the 24h time representation of the date
    */
    public static long findHourFromDate(Date date){
        SimpleDateFormat sDay = new SimpleDateFormat("k");
        String out = sDay.format(date);
        return Long.parseLong(out);
    }

    /*
     * Finds the minutes of the time of the date object
     * Useful for creating custom date objects
     * @param date the date being tested
     * @return the minutes of the date object
     */
    public static long findMinuteFromDate(Date date){
        SimpleDateFormat sDay = new SimpleDateFormat("m");
        String out = sDay.format(date);
        return Long.parseLong(out);
    }
}
