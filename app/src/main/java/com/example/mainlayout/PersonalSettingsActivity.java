package com.example.mainlayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Personal Settings Activity
 *
 * Handles the personal settings layout
 *
 * Author: Group 25
 * Version: 1.0
 */
public class PersonalSettingsActivity extends AppCompatActivity {

    UserSettings settings;
    boolean isAustralian;
    EditText editTextName;
    EditText editTextDate;

    SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Create
     * Unpack settings from intent
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState){

      super.onCreate(savedInstanceState);
      setContentView(R.layout.personal_settings);
      this.settings = (UserSettings)getIntent().getSerializableExtra("settings");
      System.out.println(this.settings);
      editTextName=findViewById(R.id.personal_name);
      editTextDate=findViewById(R.id.personal_birth);

      editTextName.setText(settings.getName());
      editTextDate.setText(sdf.format(settings.getDob()));

      RadioButton yes = (RadioButton)findViewById(R.id.radio_button1);
      RadioButton no = (RadioButton)findViewById(R.id.radio_button2);

      if (settings.getAustralianResidency()){
          yes.setChecked(true);
          no.setChecked(false);
      } else {
          yes.setChecked(false);
          no.setChecked(true);
      }
    }

    /**
     * Handle return to user_settings layout
     * @param view
     */
    public  void backToSettingPage(View view){
      Intent intent = new Intent(this, UserSettingsActivity.class);
      intent.putExtra("settings", settings);

      setResult(UserSettingsActivity.EDIT_SETTINGS, intent);
      finish();
    }

    /**
     * Save new values
     * @param view
     */
    public void saveValue(View view){

      this.settings.setAustralianResidency(isAustralian);
      this.settings.setName(editTextName.getText().toString());
      String dateStr=editTextDate.getText().toString();
      try {

        String [] arr=dateStr.split("/");
        int day=Integer.valueOf(arr[0]);
        int month=Integer.valueOf(arr[1]);

        if (day>31 || month >12){

          Toast toast=  Toast.makeText(this,"Day or Month format error!",
              Toast.LENGTH_LONG);
          toast.setGravity(Gravity.CENTER, 0, 0);
          toast.show();
                  return;
          }
              this.settings.setDob(sdf.parse(editTextDate.getText().toString()));

          } catch (ParseException e) {
              e.printStackTrace();
              Toast toast= Toast.makeText(this,"Date format error!",
                      Toast.LENGTH_LONG);
              toast.setGravity(Gravity.CENTER, 0, 0);
              toast.show();
              return;
          }
          Toast.makeText(this,"Saved successfully!",
                  Toast.LENGTH_LONG).show();
          backToSettingPage(view);
    }

    /**
     * Handle isAustralian click
     * @param view
     */
    public void yesClick(View view){
        isAustralian=true;
    }

    /**
     * Handle isAustralian click
     * @param view
     */
    public void noClick(View view){
        isAustralian=false;
    }
    //backend for personal_settings.xml
}
