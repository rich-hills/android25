package com.example.mainlayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * User Settings Activity
 * Activity class to handle operations of the User Settings layout
 *
 * Author: Group 25
 * Version: 1.0
 */
public class UserSettingsActivity extends AppCompatActivity {

    protected static final int EDIT_JOB = 101;
    protected static final int EDIT_MEDICARE = 102;
    protected static final int EDIT_HECS = 103;
    protected static final int EDIT_SUPER = 104;
    protected static final int EDIT_PIN = 105;
    protected static final int EDIT_FINANCE = 106;
    protected static final int EDIT_SETTINGS = 107;

    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInAccount account;
    public static final int RC_SIGN_IN = 301;

    UserSettings settings;
    UserDataStore dataStore;

    /**
     * Creation
     * Unpack the user settings and datastore, and initialise FireBase
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_settings);

        this.settings = (UserSettings)getIntent().getSerializableExtra("settings");
        this.dataStore = (UserDataStore)getIntent().getSerializableExtra("datastore");

        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        account = GoogleSignIn.getLastSignedInAccount(this);

        if (account != null){
            Button load = (Button)findViewById(R.id.googleLoadBtn);
            load.setVisibility(View.VISIBLE);
            Button save = (Button)findViewById(R.id.googleBackupBtn);
            save.setVisibility(View.VISIBLE);
            Button logIn = (Button)findViewById(R.id.googleSignInbtn);
            logIn.setVisibility(View.INVISIBLE);
        } else {
            Button load = (Button)findViewById(R.id.googleLoadBtn);
            load.setVisibility(View.INVISIBLE);
            Button save = (Button)findViewById(R.id.googleBackupBtn);
            save.setVisibility(View.INVISIBLE);
            Button logIn = (Button)findViewById(R.id.googleSignInbtn);
            logIn.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Confirm new user settings, and return to main activity
     * @param v
     */
    public void saveBackOnClick(View v) {
        // Write Changes
        Intent intentSettings = new Intent(this, MainActivity.class);
        Log.d("Changing Layout", "Settings->Main");
        intentSettings.putExtra("settings", settings);
        setResult(1000, intentSettings);
        finish();
    }

    /**
     * Go to edit jobs activity
     * @param v
     */
    public void editJobsOnClick(View v){
        Intent intent = new Intent(this, JobEditorActivity.class);
        intent.putExtra("Jobs", settings.getJobs());
        startActivityForResult(intent, EDIT_JOB);
    }

    /**
     * Go to edit medicare activity
     * @param v
     */
    public void editMedicareOnClick(View v){
        Intent intentSettings = new Intent(this, MedicareSettingsActivity.class);
        Log.d("Changing Layout", "Settings->HecsSettingsActivity");
        intentSettings.putExtra("settings", settings);
        startActivityForResult(intentSettings, EDIT_MEDICARE);

    }

    /**
     * Go to edit HECs activity
     * @param v
     */
    public void editHECSOnClick(View v){
        Intent intentSettings = new Intent(this, HecsSettingsActivity.class);
        Log.d("Changing Layout", "Settings->HecsSettingsActivity");
        intentSettings.putExtra("settings", settings);
        startActivityForResult(intentSettings, EDIT_HECS);

    }

    /**
     * Go to edit super activity
     * @param v
     */
    public void editSuperOnClick(View v){
        Intent intentSettings = new Intent(this, SuperSettingsActivity.class);
        Log.d("Changing Layout", "Settings->HecsSettingsActivity");
        intentSettings.putExtra("settings", settings);
        startActivityForResult(intentSettings, EDIT_SUPER);

    }

    /**
     * Go to edit user details activity
     * @param view
     */
    public void editUserDetailsOnClick(View view) {
        Intent intentSettings = new Intent(this, PersonalSettingsActivity.class);
        Log.d("Changing Layout", "Settings->HecsSettingsActivity");
        intentSettings.putExtra("settings", settings);
        startActivityForResult(intentSettings, EDIT_SETTINGS);

    }

    /**
     * Go to edit pin activity
     * @param view
     */
    public void editPinOnClick(View view) {
        Intent gotoEditPin = new Intent(this, EditPinActivity.class);
        gotoEditPin.putExtra("oldPin", settings.getPin());
        startActivityForResult(gotoEditPin, EDIT_PIN);
    }

    /**
     * Go to edit finance acitivty
     * @param view
     */
    public void editFinanceOnClick(View view) {
        Intent intent = new Intent(this, EditFinancialInformationActivity.class);
        intent.putExtra("settings", this.settings);
        startActivityForResult(intent, EDIT_FINANCE);
    }

    /**
     * Handle results of edit activities.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView warning = (TextView) findViewById(R.id.settingsChangeWarn);
        warning.setText("");
        if(resultCode==EDIT_SETTINGS){
            this.settings = (UserSettings)data.getSerializableExtra("settings");
        }
        if(resultCode==EDIT_SUPER){
            this.settings = (UserSettings)data.getSerializableExtra("settings");
        }
        if(resultCode==EDIT_HECS){
            this.settings = (UserSettings)data.getSerializableExtra("settings");
        }
        if(resultCode==EDIT_MEDICARE){
            this.settings = (UserSettings)data.getSerializableExtra("settings");
        }
        if(requestCode==EDIT_PIN && resultCode==1050) {
            String newPin = data.getStringExtra("newPin");
            this.settings.setPin(newPin);
            warning.setText(R.string.setting_changed_warn);
        }
        if (requestCode == EDIT_JOB){
            this.settings.setJobs((ArrayList<Job>)data.getSerializableExtra("Jobs"));
        }
        if (requestCode == EDIT_FINANCE && resultCode == EDIT_FINANCE){
            this.settings = (UserSettings)data.getSerializableExtra("settings");
        }
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    /**
     * Handle firebase signin click
     * @param v
     */
    public void signInClick(View v) {

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * Handle firebase signin
     * @param completedTask
     */
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        try {
            account = completedTask.getResult(ApiException.class);

            AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("E: ", "signInWithCredential:success");
                                FirebaseUser user = mAuth.getCurrentUser();

                                Button load = (Button)findViewById(R.id.googleLoadBtn);
                                load.setVisibility(View.INVISIBLE);
                                Button save = (Button)findViewById(R.id.googleBackupBtn);
                                save.setVisibility(View.INVISIBLE);
                                Button logIn = (Button)findViewById(R.id.googleSignInbtn);
                                logIn.setVisibility(View.VISIBLE);

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("E: ", "signInWithCredential:failure", task.getException());
                            }

                            // ...
                        }
                    });

            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Error:", "signInResult:failed code=" + e.getStatusCode());
        }

    }

    /**
     * Handle firebase backup button
     * @param v
     */
    public void backupClick(View v){

        Log.e("User", mAuth.getCurrentUser().getUid());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String userId = mAuth.getCurrentUser().getUid();
        DatabaseReference myRef = database.getReference("/users/" + userId);

        Gson gson = new Gson();
        String userSettings = gson.toJson(this.dataStore);

        Log.e("Usser settings: ", userSettings);
        myRef.setValue(userSettings);

    }

    /**
     * Handle firebase load
     * @param v
     */
    public void loadFromBackupClick(View v){

        Log.e("User", mAuth.getCurrentUser().getUid());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String userId = mAuth.getCurrentUser().getUid();
        DatabaseReference myRef = database.getReference("/users/" + userId);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String sDataStore = dataSnapshot.getValue(String.class);
                Gson gson = new Gson();
                dataStore = gson.fromJson(sDataStore, UserDataStore.class);

                Intent intent = new Intent();
                intent.putExtra("datastore", dataStore);
                setResult(MainActivity.LOAD_FROM_BACKUP, intent);
                finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}

        });

    }


}
