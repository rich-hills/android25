package com.example.mainlayout;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * User Settings Class
 * Class to store user settings
 *
 * Author: Group 25
 * Version: 1.0
 */
public class UserSettings implements Serializable {

  private ArrayList<Job> jobs;
  private double annual_income;
  private Date dob;
  private String pin;

  private String medicare_exemption = "none";
  private boolean australian_resident = true;
  private boolean hecs = false;
  private double super_rate = 0.095;
  private String name;

  public UserSettings(String name,double income, Date dob){
    this.jobs = new ArrayList<Job>();
    this.annual_income = income;
    this.dob = dob;
    this.name = name;
  }

  public UserSettings(String name, String pin, String wage, String d) {
    this.name = name;
    this.pin = pin;
    try {
      this.dob = new SimpleDateFormat("dd/mm/yyyy").parse(d);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.jobs = new ArrayList<Job>();
    addJob("Job", Double.parseDouble(wage));
  }

  public double getAnnualIncome(){
    return this.annual_income;
  }

  public void setAnnualIncome(double annual_income){
    this.annual_income = annual_income;
  }

  public Date getDob(){return dob;}

  public void setDob(Date age){
    this.dob = age;
  }

  public ArrayList<Job> getJobs(){
    return this.jobs;
  }
  public Job getJob(String job_name){
    for (Job job : jobs){
      if(job.getName().equals(job_name)){
        return job;
      }
    }
    return null;
  }

  public void addJob(String job_name, double hourly_wage){
    jobs.add(new Job(job_name, hourly_wage, false));
  }

  public void setJobs(ArrayList<Job> jobs){
    this.jobs = jobs;
  }


  public void setMedicareExemptionStatus(String status){
    this.medicare_exemption = status;
  }

  public String getMedicareStatus(){
    return this.medicare_exemption;
  }

  public void setAustralianResidency(boolean residency){
    this.australian_resident = residency;
  }

  public boolean getAustralianResidency(){
    return this.australian_resident;
  }

  public void setHecsStatus(boolean status){
    this.hecs = status;
  }

  public boolean getHecsStatus(){
    return this.hecs;
  }

  public String getPin() {  return this.pin;  }

  public void setPin(String nP) {this.pin=nP;}

  public String getName() {return this.name;}

  public void setName(String n) {this.name = n;}

  public double getSuper_rate(){return this.super_rate;}

  public void setSuperRate(Double super_rate){this.super_rate = super_rate;}

}
