package com.example.mainlayout;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

/**
 * User Signup Activity
 * Activity to handle signup for new users.
 *
 * Author: Group 25
 * Version: 1.0
 */

public class UserSignupActivity extends AppCompatActivity {

    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    public static final int READFILES_PERMISSION_REQUEST_CODE = 4;

    /**
     * Create
     * Check permissions, setup textfield listeners for dynamic error feedback.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkPermissionForExternalStorage() ||
                !checkPermissionForReadfiles()){
            requestPermissionForExternalStorage();
            requestPermissionForReadfiles();
        }

        setContentView(R.layout.user_signup);

        // Setup Listeners
        // Pin
        TextInputEditText pinField = (TextInputEditText) findViewById(R.id.signupPass);
        pinField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePin(false);
            }
        });

        //Pin Conf
        TextInputEditText pinConfField = (TextInputEditText) findViewById(R.id.signupConfPass);
        pinConfField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePin(true);
            }
        });

        // Base Pay
        TextInputEditText basePayField = (TextInputEditText) findViewById(R.id.signupBasePay);
        basePayField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePayRate();
            }
        });

        // Name
        TextInputEditText nameField = (TextInputEditText) findViewById(R.id.signupUsrname);
        nameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validateName();
            }
        });

        // DOB
        TextInputEditText dobField = (TextInputEditText) findViewById(R.id.signupDOB);
        dobField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validateDOB();
            }
        });

    }

    /**
     * Ensure DOB is valid and formatted correctly
     * @return
     */
    private boolean validateDOB() {
        TextInputEditText dobField = (TextInputEditText) findViewById(R.id.signupDOB);
        TextView err = (TextView) findViewById(R.id.signupDOBErr);
        err.setText("");
        try {
            String dob = Objects.requireNonNull(dobField.getText(), "null DOB").toString();
            String[] dobSplit = dob.split("/");
            if(dobSplit[2].length() != 4) {
                err.setText(R.string.dob_err_b);
                return false;
            }
            Date test = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(dob);
            Date now = new Date();
            if(test.compareTo(now) > 0) {
                err.setText(R.string.dob_err_a);
                return false;
            }
        } catch(Exception e) {
            Log.d("ValidateDOB", e.getStackTrace().toString());
            err.setText(R.string.dob_err_b);
            return false;
        }
        return true;

    }

    /**
     * Ensure pay is valid and formatted correctly
     * @return
     */
    private boolean validatePayRate() {
        TextView err = (TextView) findViewById(R.id.signupBasePayErr);
        err.setText("");
        TextInputEditText basePayField = (TextInputEditText) findViewById(R.id.signupBasePay);
        String basePay;
        try {
            basePay = Objects.requireNonNull(basePayField.getText(),
                    "Null Pay Rate").toString();
        } catch(Exception e) {
            Log.d("validatePayRate",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return false;
        }
        try {
            Double.parseDouble(basePay);
        } catch (Exception e) {
            err.setText(R.string.base_pay_err);
            return false;
        }
        return true;
    }

    /**
     * Ensure name is not Null
     * @return
     */
    private boolean validateName() {
        TextView err = (TextView) findViewById(R.id.signupUsrnameErr);
        err.setText("");
        TextInputEditText nameField = (TextInputEditText) findViewById(R.id.signupUsrname);
        String name;
        try {
            name = Objects.requireNonNull(nameField.getText(),
                    "Null Name").toString();
        } catch (Exception e) {
            Log.d("validateName",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return false;
        }
        if(name.length() < 1) {
            err.setText(R.string.signupNameErr);
            return false;
        }
        return true;
    }

    /**
     * Ensure pin is valid, i.e. 6 digits long, or confirm pins match
     * @param conf - are we confirming pin match
     * @return
     */
    private boolean validatePin(boolean conf) {
        TextView err = (TextView) findViewById(R.id.passwordErr);
        err.setText("");
        String pin;
        String pinConf;
        if(conf) {
            TextInputEditText pinField = (TextInputEditText) findViewById(R.id.signupPass);
            TextInputEditText pinConfField = (TextInputEditText) findViewById(R.id.signupConfPass);
            try {
                pin = Objects.requireNonNull(pinField.getText(), "Null Pin").toString();

                pinConf = Objects.requireNonNull(pinConfField.getText(),
                        "Null PinConf").toString();
            } catch (Exception e) {
                Log.d("validatePin",e.getMessage()+Arrays.toString(e.getStackTrace()));
                return false;
            }
            if(!pin.equals(pinConf)) {
                err.setText(R.string.pin_conf_error);
                return false;
            }
        } else {
            TextInputEditText pinField = (TextInputEditText) findViewById(R.id.signupPass);
            try {
                pin = Objects.requireNonNull(pinField.getText(), "Null pin").toString();
            } catch (Exception e) {
                Log.d("validatePin",e.getMessage()+Arrays.toString(e.getStackTrace()));
                return false;
            }
            if(pin.length() > 6) {
                err.setText(R.string.pin_length_error_a);
                return false;
            } else if (pin.length() < 6) {
                err.setText(R.string.pin_length_error_b);
                return false;
            }
        }
        return true;
    }

    /**
     * Handle signup onclick.
     * Extract data from fields, validate them, then create user data, and pass it to the main activity
     * @param v
     */
    public void signupOnClick(View v) {
        Log.d("Signup Alert!", "Button Pressed");

        TextInputEditText usernameField = (TextInputEditText) findViewById(R.id.signupUsrname);
        TextInputEditText pinField = (TextInputEditText) findViewById(R.id.signupPass);
        TextInputEditText pinConfField = (TextInputEditText) findViewById(R.id.signupConfPass);
        TextInputEditText basePayField = (TextInputEditText) findViewById(R.id.signupBasePay);
        TextInputEditText dobField = (TextInputEditText) findViewById(R.id.signupDOB);
        String username;
        String pin;
        String pinConf;
        String pay;
        String dob;
        try {
            username = Objects.requireNonNull(usernameField.getText()
                    , "Null name").toString();
            pin = Objects.requireNonNull(pinField.getText(), "Null pin").toString();
            pinConf = Objects.requireNonNull(pinConfField.getText()
                    ,"null Pin Conf").toString();
            pay = Objects.requireNonNull(basePayField.getText(), "Null pay").toString();
            dob = Objects.requireNonNull(dobField.getText(), "null dob").toString();
            Log.d("Signup Alert!", "Username was: "+username);
        } catch(Exception e) {
            Log.d("SignupErr",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return;
        }

        if(validateName() && validatePayRate() &&
                validatePin(false) && validatePin(true) && validateDOB()){
            // Format for GSON and save
            UserSettings us = new UserSettings(username, pin, pay, dob);
            UserDataStore data = new UserDataStore(us);
            Gson gson = new Gson();
            String json = gson.toJson(data);

            // Write IV and Data
            FileSystemHelper fs = new FileSystemHelper(getApplicationContext());

            if(!fs.write(json, "ud.txt", getFilesDir().getAbsolutePath())) {
                Log.d("Write UserData", "couldn't write UserData");
                Toast.makeText(this,"Something went wrong, and we weren't able to save your data"
                        , Toast.LENGTH_SHORT).show();
                return;
            }

            // Move to main view
            Intent intentSignedUP = new Intent(this, MainActivity.class);
            Log.d("Changing Layout", "Signup->Main");
            intentSignedUP.putExtra("UserDataStore", data);
            this.startActivity(intentSignedUP);
        } else {
            Toast.makeText(this,"Please check the fields to make sure they're complete!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkPermissionForReadfiles(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public boolean checkPermissionForExternalStorage(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public void requestPermissionForExternalStorage(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            Toast.makeText(this, "External Storage permission needed. Please allow in App Settings for functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            System.out.print("get storage permission");
        }
    }

    public void requestPermissionForReadfiles(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
            Toast.makeText(this, "Read files permission needed. Please allow in App Settings for functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},READFILES_PERMISSION_REQUEST_CODE);
        }
    }

}
