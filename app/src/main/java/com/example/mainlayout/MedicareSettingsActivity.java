package com.example.mainlayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.ParseException;

public class MedicareSettingsActivity extends AppCompatActivity {

    UserSettings settings;

    RadioButton exemption;
    RadioButton normal;
    RadioButton half;
    String value;


    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medicare_settings);
        this.settings = (UserSettings)getIntent().getSerializableExtra("settings");
        exemption=findViewById(R.id.medicare_exemption);
        normal=findViewById(R.id.medicare_normal);
        half=findViewById(R.id.medicare_exemptionHalf);


        switch (settings.getMedicareStatus()) {
            case "none":
                normal.toggle();
                break;
            case "half":
                half.toggle();
                break;
            case "full":
                exemption.toggle();
                break;
        }

        exemption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              if (isChecked){
                  value="full";
              }
          }
      });
        normal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              if (isChecked){
                  value="none";
              }
          }
      });
        half.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    value="half";
                }
            }
        });
    }

    public void backToSettingPage(View view){

        Intent intent = new Intent(this, UserSettingsActivity.class);
        intent.putExtra("settings", settings);

        setResult(UserSettingsActivity.EDIT_MEDICARE, intent);
        finish();
    }

    public void saveValue(View view){

        this.settings.setMedicareExemptionStatus(value);
        Log.d("Saving Medicare", value);
        backToSettingPage(view);
    }

    //code for medicare_settings.xml
}
