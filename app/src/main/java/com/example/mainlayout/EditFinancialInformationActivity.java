package com.example.mainlayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.textfield.TextInputEditText;
import java.util.Arrays;
import java.util.Objects;

/*
 * Handler for the activity which enables a user to edit their financial information
 */
public class EditFinancialInformationActivity extends AppCompatActivity {

    /*
     * The user settings being changed
     */
    UserSettings settings;
    /*
     * The text field which enables the user to edit their annual income
     */
    TextInputEditText annual;

    /*
     * Called on creation of the activity
     * Extracts user settings from the sent intent
     * Sets the text field to that of the user settings annual income
     */
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_finance);

        this.settings = (UserSettings)getIntent().getSerializableExtra("settings");
        annual = (TextInputEditText)findViewById(R.id.editAnnualIncome);
        annual.setText(String.valueOf(settings.getAnnualIncome()));
    }

    /*
     * Called on click of the save button
     */
    public void save (View v){

        // constructs a confirmation dialog to confirm changes in case of missclick
        AlertDialog.Builder builder = new AlertDialog.Builder(EditFinancialInformationActivity.this);
        builder.setTitle("")
                .setMessage("Save information?")
                .setPositiveButton("Yes", new
                        DialogInterface.OnClickListener() {
                            // on clicking the yes option
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String a; // to represent the string in the annual income field
                                try {
                                    // checks the annual income field contains text
                                    a = Objects.requireNonNull(annual.getText(), "Invalid income").toString();
                                } catch (Exception e){
                                    // cancel the save if object is null
                                    return;
                                }
                                // checks the income is a valid double
                                if (validateRate()) {
                                    // if the income is valid, sets the annual income to the value
                                    // in the text field
                                    settings.setAnnualIncome(Double.parseDouble(a));
                                    // stores the updated settings in an intent to return
                                    Intent intent = new Intent();
                                    intent.putExtra("settings", settings);
                                    setResult(UserSettingsActivity.EDIT_FINANCE, intent);
                                    finish();
                                }
                                // will remain on current activity if invalid income
                            }
                        })
                .setNegativeButton("No", new
                        // on no click, cancel the dialog and remain on current activity
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });

        builder.create().show();

    }

    /*
     * Called on click of the cancel button
     */
    public void cancel (View v){

        // constructs a confirmation dialog to confirm the exit in case of missclick
        AlertDialog.Builder builder = new AlertDialog.Builder(EditFinancialInformationActivity.this);
        builder.setTitle("")
                .setMessage("Do you wish to cancel?")
                .setPositiveButton("Yes", new
                        DialogInterface.OnClickListener() {
                            // on yes click, closes activity
                            public void onClick(DialogInterface dialogInterface, int i) {

                                // returns user settings object by adding it to the intent
                                Intent intent = new Intent();
                                intent.putExtra("settings", settings);
                                setResult(UserSettingsActivity.EDIT_FINANCE, intent);
                                finish();

                            }
                        })
                .setNegativeButton("No", new
                        DialogInterface.OnClickListener() {
                            // on no click, remains on the activity
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });

        builder.create().show();

    }

    /*
     * confirms that the input field is a valid double
     * @return true if the field is valid, false if not
     */
    private boolean validateRate() {
        TextInputEditText basePayField = (TextInputEditText) findViewById(R.id.editAnnualIncome);
        String basePay;
        try {
            // checks if the field is non null
            basePay = Objects.requireNonNull(basePayField.getText(),
                    "Null Pay Rate").toString();
        } catch(Exception e) {
            Log.d("validatePayRate",e.getMessage()+ Arrays.toString(e.getStackTrace()));
            return false;
        }
        try {
            // attempts to parse the text as a double
            Double.parseDouble(basePay);
        } catch (Exception e) {
            // exception triggers if text is not a valid double
            Toast.makeText(this,"Make sure to enter a penalty",Toast.LENGTH_LONG);
            return false;
        }
        // returns true if double is valid
        return true;
    }
}
