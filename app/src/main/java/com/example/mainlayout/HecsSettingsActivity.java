package com.example.mainlayout;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

/*
 * Handler of the edit HECS settings activity
 */
public class HecsSettingsActivity extends AppCompatActivity {

    /*
     * The UserSettings object of the user whose settings are being changed
     */
    UserSettings settings;
    /*
     * Value containing whether the user has HECS or not
     */
    boolean chooseResult;
    /*
     * Radio button that is ticked if user selects they have a HECS debt
     */
    RadioButton yes;
    /*
     * Radio button that is ticked if user selects they do not have a HECS debt
     */
    RadioButton no;

    /*
     * Method called upon creation of activity
     * Extracts settings from intent then configures radio buttons based of current settings
     */
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hecs_settings);
        // extracts settings from intent
        this.settings = (UserSettings)getIntent().getSerializableExtra("settings");
        // find the radio buttons encoded in layout
        yes = (RadioButton)findViewById(R.id.radio_button1);
        no = (RadioButton)findViewById(R.id.radio_button2);

        // set the radio buttons according to previous HECS settings
        if (settings.getHecsStatus()){
            yes.setChecked(true);
            no.setChecked(false);
        } else {
            yes.setChecked(false);
            no.setChecked(true);
        }
    }


    /*
     * called upon click of the cancel button
     * closes activity and returns to edit user settings activity
     */
    public void backToSettingPage(View view){
        // create an intent with user settings and finish activity
        Intent intent = new Intent(this, UserSettingsActivity.class);
        intent.putExtra("settings", settings);
        setResult(UserSettingsActivity.EDIT_HECS, intent);
        finish();
    }

    /*
     * called upon click of save button
     * closes activity and returns to edit user settings activity with updated HECS status
     */
    public void saveValue(View view){
        // updates the HECS status based on users selection
        this.settings.setHecsStatus(chooseResult);
        Toast.makeText(this,"Saved successfully!",
                Toast.LENGTH_LONG).show();
        // calls close of activity
        backToSettingPage(view);
    }

    /*
     * called upon click of yes button
     * sets value of chosenResult to true indicating user has HECS debt
     */
    public void yesClick(View view){
        chooseResult=true;
    }

    /*
     * called upon click of no button
     * sets value of chosenResult to false indicating user has no HECS debt
     */
    public void noClick(View view){
        chooseResult=false;
    }
}
