package com.example.mainlayout;

import android.util.Log;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.mariuszgromada.math.mxparser.*;

public class TaxCalculator {

    JsonReader myJsonReader;
    private Map<String, String> myTaxSchedule = new HashMap<>();
    public String defaultTaxSchedule = "Resident tax rates 2020–21"; //  Resident tax rates 2019–2020

    // USER SETTINGS
    private int taxableIncome = 0;
    private String bracketString = "0-18200";//default lowest tax bracket
    private String myTaxEquation = ""; //default Nil tax
    private static final String UPPER_BRACKET = "180001-m";

    public Boolean StudentDebt = false; //default no student debt
    private Object studentLoanRepayment; // initialize only if used
    private String hecsSchedule = "HECS rates 2020-21";
    private double medicareLevy = 0; //default is 0
    private String medicareExemption = "none";

    public void setMedicareLevy(String levyPercent){
        switch (levyPercent){
            case "0%":
                //default case
                break;
            case "1%":
                medicareLevy=0.01;
                break;
            case "1.25%":
                medicareLevy=0.0125;
                break;
            case "1.5%":
                medicareLevy=0.015;
                break;
            default:
                System.out.println(levyPercent + " MEDICARE LEVY can only be '0%','1%','1.25%','1.5%' ");
                break;
        }
    }

    public void setMedicareExemption(String MedicareExemption){
        if(MedicareExemption=="full"||MedicareExemption=="half"||MedicareExemption=="none"){
            medicareExemption = MedicareExemption; }
                else{System.out.println("invalid choice for medicare exemption");}
    }

    public double getMedicareLevy(){
        if (taxableIncome < 90000){
            return 0;
        }
        switch (medicareExemption){
            case "full":
                medicareLevy = 0;
                break;
            case "half":
                medicareLevy = taxableIncome*0.01;
                break;
            case "none":
                medicareLevy = taxableIncome*0.02;
                break;
            default:
                medicareLevy = taxableIncome*0.02;
                break;
        }
        return medicareLevy;
    }


    //  assuming here that the % is already set for the MLS levy (no private health insurance)
    public double simpleMLS(String levyPercent){
        setMedicareLevy(levyPercent);
        double output = medicareLevy*taxableIncome;
        return output;
    }


    // public function to swap the HECS schedule years using existing json files
    public void setSchedule_HECS(String scheduleName){
        switch (scheduleName){
            case "HECS20-21":
                hecsSchedule = "HECS20-21";
                break;
            case "HECS19-20":
                hecsSchedule = "HECS19-20";
                break;
            default:
                System.out.println(scheduleName + ".json  is not a known file");
        }
    }

    // switch statements may not be ideal, however the json filenames have limited scope
    public void setSchedule_TAX(String scheduleName){
        switch (scheduleName){
            case "ResidentTax19-20":
                defaultTaxSchedule = "ResidentTax19-20";
                break;
            case "ResidentTax20-21":
                hecsSchedule = "ResidentTax20-21";
                break;
            default:
                System.out.println(scheduleName + ".json  is not a known file");
        }
    }

    // this can be extended as future HECS compulsory repayment schedules are created
    private Object compulsoryHECS_repayment(){
        //double output = 0;
        String hecs = "\n{\n\"HECS rates 2020-21\" : \n {\n\"0-45881\": \"0\",\n\"46620-53826\": \"T*0.01\",\n\"53827-57055\": \"T*0.02\",\n\"57056-60479\": \"T*0.025\",\"60480-64108\": \"T*0.03\",\n\"64109-67954\": \"T*0.035\",\"67955-72031\": \"T*0.04\",\n\"72032-76354\": \"T*0.045\",\n\"76355-80935\": \"T*0.05\",\n\"80936-85792\": \"T*0.055\",\n\"85793-90939\": \"T*0.06\",\n\"90940-96396\": \"T*0.065\",\n\"96397-102179\": \"T*0.07\",\n\"102180-108309\": \"T*0.075\",\n\"108310-114707\": \"T*0.08\",\n\"114708-121698\": \"T*0.085\",\n\"121699-128999\": \"T*0.09\",\n\"129000-136739\": \"T*0.095\",\n\"136740-m\": \"T*0.1\" \n}\n\n}";
        Map<String, String> hecsMap = loadTaxSchedule(hecs);
        String incomeBracket = getGenericBracket(taxableIncome, hecsMap);
        String hecsCalculationT = hecsMap.get(incomeBracket);
        String hecsCalculation = subsituteTforY(hecsCalculationT);

        Object output = evalExpression(hecsCalculation);
        return output;
    }

    //simple tax is stand-alone and works given taxable income and myTaxSchedule is defined
    public double getSimpleTax(){
        double output = 0;
        //find the income bracket string
        bracketString=getTaxBracket(taxableIncome);
        // Get the equation for simple tax at the correct income bracket
        myTaxEquation=myTaxSchedule.get(bracketString);
        System.out.println("Unit test - myTaxEquation="+myTaxEquation+" bracketString="+bracketString);

        if(myTaxEquation==null){myTaxEquation="T";}
        String taxExpression = subsituteTforY(myTaxEquation);
        Object calculatedTax = evalExpression(taxExpression);
        String outputTax = calculatedTax.toString();
        output = Double.valueOf(outputTax);
        return output;
    }

    //eval strings as expressions
    private Object evalExpression(String s){
        //double output = 0.0;
        // not sure if is double or string??
        Object output;
        Expression e = new Expression(s);
        output = e.calculate();
        return output;
    }

    //function to substitute income Y for T in equation
    private String subsituteTforY(String inputEquation){
        String output = "";
        String income = Integer.toString(taxableIncome);
        Log.e("Input equation:", inputEquation);
        // tax free threshold
        if(inputEquation.equals("T")){
            output=income;
        }
        else if(inputEquation.length()>2){
            String[] cut_equation = inputEquation.split("T",2);
            output = cut_equation[0]+income+cut_equation[1];
        }
        else{System.out.println("invalid tax calculation equation");}

        return output;
    }

    private void setIncome(int Y){
        if(Y>=0 && Y<2147483647){taxableIncome = Y;}
        else{System.out.println("invalid income");}
    }

    private void setTaxSchedule(String taxSchedule){
        String tax = "\n{\n\"Resident tax rates 2020–21\":\n{\n\"0-18200\":\"0\",\n\"18201-45000\":\"(T-18201)*0.19\",\n\"45001-120000\":\"5092+(T-45000)*0.32\",\n\"120001-180000\":\"29467+(T-120000)*0.37\",\n\"180001-m\":\"51667+(T-18000)*0.45\"\n}\n\n}";
        try{myTaxSchedule = loadTaxSchedule(String.valueOf(tax));}
        catch(Exception e){System.out.println("something went wrong loading taxSchedule  '"+taxSchedule+"' : "+ e);}
    }

    // 'taxSchedule' is filenames = ResidentTax20-21, ResidentTax19-20, HECS19-20, HECS20-21
    private Map<String, String> loadTaxSchedule(String taxSchedule) {

        //pick the json taxSchedule
        Map<String, String> output = new HashMap<>(); // init empty map

        try{

            myJsonReader = new JsonReader(new StringReader(taxSchedule));
            //myJsonReader.beginObject();

            String taxBracket = "";
            String taxEquation = "";
            while (myJsonReader.hasNext()) {



                JsonToken nextToken = myJsonReader.peek();
                nextToken = myJsonReader.peek();

                if (nextToken == null){
                    break;

                }
                //nextToken == NAME, STRING
                if(JsonToken.BEGIN_OBJECT.equals(nextToken)){

                    myJsonReader.beginObject();

                } else if(JsonToken.BEGIN_ARRAY.equals(nextToken)) {

                    myJsonReader.beginArray();

                } else if(JsonToken.NAME.equals(nextToken)){
                        String name = myJsonReader.nextName();
                        taxBracket = name;
                        Log.e("Name:", name);

                } else if(JsonToken.STRING.equals(nextToken)){
                    String value =  myJsonReader.nextString();
                    //System.out.println(value + " calculation where T is taxable income");
                    taxEquation = value;
                    Log.e("Name:", value);
                    output.put(taxBracket, taxEquation);


                } else if(JsonToken.NUMBER.equals(nextToken)){

                    long value =  myJsonReader.nextLong();
                    System.out.println(value + "alpha_test3");

                }


            }
            myJsonReader.endObject();
            myJsonReader.close();
            System.out.println(taxSchedule + " loaded successfully!");
        } catch (Exception e){e.printStackTrace();} // file not found exception

        //return hashmap
        return output;
    }

    // locate income bracket from map based on income(Y) - eg: "121699-128999":"T*0.09"
    private String getGenericBracket(int Y, Map<String,String> bracketMap){
        String output = "";
        int upperBracket = 0;
        int lowerBracket = 0;
        String[] brackets = bracketMap.keySet().toArray(new String[0]);
        for(String s: brackets){
            String[] highLow = s.split("-",2);

            int testLow = Integer.valueOf(highLow[0]);
            int testHigh = Integer.valueOf(highLow[1]);

            if(Y>=testLow && Y <= testHigh){
                upperBracket = testHigh;
                lowerBracket = testLow;
            }
        }

        output = lowerBracket +"-"+ upperBracket;
        return output;
    }

    private String getTaxBracket(int Y){
        String output = "";
        int upperBracket = 0;
        int lowerBracket = 0;
        String[] brackets = myTaxSchedule.keySet().toArray(new String[0]);
        Arrays.sort(brackets);
        for(String s: brackets){
            String[] highLow = s.split("-",2);

            int testLow = Integer.valueOf(highLow[0]);

            if (highLow[1].equals("m")){
                highLow[1] = "2147483647";
            }
            int testHigh = Integer.valueOf(highLow[1]);


            if(Y>=testLow && Y <= testHigh){
                upperBracket = testHigh;
                lowerBracket = testLow;

                if (lowerBracket == 180001 && upperBracket == 2147483647){
                    output = lowerBracket +"-m";
                    return output;
                }

            }
        }

        output = lowerBracket +"-"+ upperBracket;
        return output;
    }

    //basic constructor is redundant
    public TaxCalculator(int Y){
        setIncome(Y);

        setTaxSchedule(defaultTaxSchedule);
        //Determine the tax bracket for income level Y
        bracketString=getTaxBracket(Y);
        // Get the equation for simple tax at the correct income bracket
        myTaxEquation=myTaxSchedule.get(bracketString);

        Log.e("My income: ", String.valueOf(Y));
        Log.e("Bracket string: ", bracketString);
        Log.e("My tax equation: ", myTaxEquation);

        if(myTaxEquation==null){myTaxEquation="T";}
        String taxExpression = subsituteTforY(myTaxEquation);
        Object calculatedTax = evalExpression(taxExpression);

        System.out.println("$"+Y + "|| tax-bracket{"+bracketString + "} || simpleTax = "+taxExpression +" = "+calculatedTax);
    }

    public double getStudentLoanRepayment(){
        double output = 0;
        try{

            String loanString = studentLoanRepayment.toString();
            output = Double.valueOf(loanString);
            System.out.println("annual student loan repayment amount is $" + output);

            }   catch(Exception e) {System.out.println(e+" error/student loan = false");}
        return output;
    }

    public TaxCalculator(int Y, boolean HECS, String medicare_exemption){
        setIncome(Y);
        setMedicareExemption(medicare_exemption);

        if(HECS==true){
            StudentDebt = true;
            studentLoanRepayment = compulsoryHECS_repayment();
        }

        //use default tax schedule 2020-2019
        setTaxSchedule(defaultTaxSchedule);
        //Determine the tax bracket for income level Y
        double mySimpleTax = getSimpleTax();

        System.out.println("$"+Y + "|| tax-bracket{"+bracketString + "} || simpleTax = $"+getSimpleTax()+" " +
                "|| compulsory student loan repayment = $"+getStudentLoanRepayment()+" " +
                    "|| Basic Medicare Levy exemption:("+medicareExemption+") $"+getMedicareLevy());
    }


}
