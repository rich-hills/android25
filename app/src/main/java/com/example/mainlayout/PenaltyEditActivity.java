package com.example.mainlayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * Penalty Edit Activity
 *
 * Activity to handle the penalty editor layout
 *
 * Author: Group 25
 * Version: 1.0
 */
public class PenaltyEditActivity extends AppCompatActivity {

    protected ArrayList<String> validDates;
    protected ArrayAdapter<String> spinnerAdapter;
    protected Spinner spinner;

    /**
     * Create
     * Set up penalty spinner
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.penalty_editor);

        validDates = new ArrayList<String>();
        validDates.add("Monday");
        validDates.add("Tuesday");
        validDates.add("Wednesday");
        validDates.add("Thursday");
        validDates.add("Friday");
        validDates.add("Saturday");
        validDates.add("Sunday");
        spinner = (Spinner)findViewById(R.id.addPenaltyDay);
        spinnerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, validDates);
        spinner.setAdapter(spinnerAdapter);

    }

    /**
     * Handle penalty creation
     * @param view
     */
    public void createPenaltyOnClick(View view) {

        Spinner dayField = (Spinner) findViewById(R.id.addPenaltyDay);
        TextInputEditText rateField = (TextInputEditText) findViewById(R.id.addPenaltyRate);
        TextInputEditText startField = (TextInputEditText) findViewById(R.id.addPenaltyStart);
        TextInputEditText endField = (TextInputEditText) findViewById(R.id.addPenaltyEnd);

        String day;
        String rate;
        String start;
        String end;

        try {
            rate = Objects.requireNonNull(rateField.getText(), "Null rate").toString();
            start = Objects.requireNonNull(startField.getText()
                    ,"null start date").toString();
            end = Objects.requireNonNull(endField.getText(), "null end date").toString();
        } catch(Exception e) {
            Log.d("SignupErr",e.getMessage()+ Arrays.toString(e.getStackTrace()));
            return;
        }

        if (validateRate() && validateDate(startField) && validateDate(endField)){

            day = dayField.getSelectedItem().toString();
            Double r = Double.parseDouble(rate.replace("+","")
                    .replace("%",""))/100;

            CustomDate startTime = new CustomDate(
                    day,
                    Long.parseLong(start.split(":")[0]),
                    Long.parseLong(start.split(":")[1])
            );

            CustomDate endTime = new CustomDate(
                    day,
                    Long.parseLong(end.split(":")[0]),
                    Long.parseLong(end.split(":")[1])
            );


            Penalty penalty = new Penalty(day, startTime,endTime,r);
            Intent intent = new Intent();
            intent.putExtra("Penalty", penalty);
            setResult(IndividualJobEditorActivity.PENALTY_ADD_CODE, intent);
            finish();

        }
    }

    /**
     * Return to user settings without saving
     * @param view
     */
    public void cancelPenaltyActivity(View view) {

        setResult(IndividualJobEditorActivity.PENALTY_CANCEL_CODE);
        finish();
    }

    /**
     * Validate penalty rate
     * @return
     */
    private boolean validateRate() {
        TextInputEditText basePayField = (TextInputEditText) findViewById(R.id.addPenaltyRate);
        String basePay;
        try {
            basePay = Objects.requireNonNull(basePayField.getText(),
                    "Null Pay Rate").toString();
        } catch(Exception e) {
            Log.d("validatePayRate",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return false;
        }
        try {
            Double.parseDouble(basePay);
        } catch (Exception e) {
            Toast.makeText(this,"Make sure to enter a penalty",Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }

    /**
     * Validate date
     * @param field
     * @return
     */
    private boolean validateDate(TextInputEditText field){

        String d;
        try {
            d = Objects.requireNonNull(field.getText(),
                    "Null date").toString();
        } catch(Exception e) {
            Log.d("validateDate",e.getMessage()+Arrays.toString(e.getStackTrace()));
            return false;
        }
        try {

            SimpleDateFormat s = new SimpleDateFormat("kk:mm");
            Date date = s.parse(d);

        } catch (Exception e) {
            Toast.makeText(this,"Time formatted incorrectly",Toast.LENGTH_LONG);
            return false;
        }
        return true;


    }


}
