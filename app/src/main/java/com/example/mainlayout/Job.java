package com.example.mainlayout;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Represents a job and its corresponding information
 */
public class Job implements Serializable {

    /*
     * A string to identify the name of the job by
     */
    private String job_name;
    /*
     * A value representing the hourly wage of the job
     */
    private double hourly_wage;
    /*
     * True if this is the users primary job, false otherwise
     */
    private Boolean primary_job;
    /*
     * A list storing all the penalty objects the job contains
     */
    private ArrayList<Penalty> penalties;

    /*
     * A value representing the overtime pay of the job
     */
    private double overtime_penalty = 0.25;

    /*
     * True if the user is a casual worker, false if not
     */
    private boolean is_casual = false;
    /*
     * Penalty rate for casual work on this job
     */
    private double casual_penalty = 0.25;

    /*
     * Constructor for the job object
     * @param name a string representing the jobs name
     * @param wage the hourly wage of the job
     * @param primary_job whether this is the users primary job or not
     */
    public Job(String name, double wage, boolean primary_job){
      this.job_name = name;
      this.hourly_wage = wage;
      this.primary_job = primary_job;
      this.penalties = new ArrayList<Penalty>();
    }

    /*
     * sets the name of the job to a new value
     * @param job_name the updated name of the job
     */
    public void setName(String job_name){
      this.job_name = job_name;
    }

    /*
     * gets the name of the job
     * @return the name of the job
     */
    public String getName(){
      return this.job_name;
    }

    /*
     * sets the wage of the job to a new value
     * @param hourly_wage the updated wage
     */
    public void setWage(double hourly_wage){
      this.hourly_wage = hourly_wage;
    }

    /*
     * gets the wage of the job
     * @return the wage of the job
     */
    public double getWage(){
      return this.hourly_wage;
    }

    /*
     * sets the primary job status of this job
     * @param primary_job true if this is the users primary job, false otherwise
     */
    public void setPrimaryJob(Boolean primary_job){
      this.primary_job = primary_job;
    }

    /*
     * returns whether this is the users primary job or not
     * @return true if primary job, false if not
     */
    public boolean getJobStatus(){
      return this.primary_job;
    }

    /*
     * sets the overtime penalty of the job
     * @param overtime_penalty a value representing the overtime penalty rate
     */
    public void setOvertimePenalty(int overtime_penalty){
      this.overtime_penalty = overtime_penalty;
    }

    /*
     * gets the overtime penalty rate of the job
     * @return the overtime penalty rate
     */
    public double getOvertimePenalty(){
      return this.overtime_penalty;
    }

    /*
     * sets the casual status of the job
     * @param is_casual true to set job to casual, false to set job to part time/full time
     */
    public void setCasual(boolean is_casual){
      this.is_casual = is_casual;
    }

    /*
     * checks whether this job is casual or not
     */
    public boolean checkCasual(){
      return is_casual;
    }

    /*
     * sets the casual penalty rate of this job
     * @param casual_penalty the casual penalty rate
     */
    public void setCasualPenalty(double casual_penalty){
      this.casual_penalty = casual_penalty;
    }

    /*
     * gets the casual penalty rate of the job
     * @return the casual penalty rate
     */
    public double getCasualPenalty(){
      return this.casual_penalty;
    }

    /*
     * gets the list of penalties associated with this job
     * @return the ArrayList of penalty objects
     */
    public ArrayList<Penalty> getPenalties(){
        return this.penalties;
    }
    /*
     * adds a new penalty object to the array by creating a new penalty object
     * @see constructor for penalty object
     * @param day a string representing a description of the first three letters of the day
     *  (ie Mon, Tue)
     * @param start_time a custom date object representing the start time of the penalty
     * @param end_time a custom date object representing the end time of the penalty
     * @param penalty_rate a value representing the penalty rate attached to the penalty
     */
    public void addPenalty(String day, CustomDate start_time, CustomDate end_time, double penalty_rate){
        penalties.add(new Penalty(day,start_time,end_time,penalty_rate));
    }
    /*
     * adds a new penalty object to the array from an existing penalty object
     * @param penalty a penalty object to add to the array
     */
    public void addPenalty(Penalty penalty){
        penalties.add(penalty);
    }
  }
