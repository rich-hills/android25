package com.example.mainlayout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

/**
 * Shift Calculator Activity
 *
 * Class to handle the calculator layout
 *
 * Author: Group 25
 * Version: 1.0
 */
public class shiftedCalculator extends Activity {
    int year;
    int month;
    int dayOfMonth;
    Shift shift;
    private LinkedList<Shift> shifts;

    TextView tv;

    /**
     * Create
     * Loads date from intent
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator);
        year = getIntent().getIntExtra("year",2020);
        month = getIntent().getIntExtra("month",01) + 1;
        dayOfMonth = getIntent().getIntExtra("dayOfMonth",1);


        tv = (TextView) findViewById(R.id.textView);
        tv.setText("The date you selected is " + dayOfMonth+"/"+month + " "+year);

    }

    /**
     * Create new shift and return it
     * @param view
     */
    public void onConfirm(View view) {

        EditText job_name =  (EditText)findViewById(R.id.jobName);
        EditText start =  (EditText)findViewById(R.id.start);
        EditText end =  (EditText)findViewById(R.id.end);
        Shift shift;

        try {

            String name = job_name.getText().toString();
            String day = String.valueOf(year)+"/"+String.valueOf(month)+"/"+String.valueOf(dayOfMonth);
            SimpleDateFormat time = new SimpleDateFormat("HH:mm:yyyy/MM/dd");
            Date s = time.parse(start.getText().toString()+":"+day);
            Date e = time.parse(end.getText().toString()+":"+day);

            shift = new Shift(name,day,s,e);

            Intent resultIntent = new Intent();
            //Log.d("Trying to add shifts", "sending data to "+resultIntent.toString());
            resultIntent.putExtra("result", shift);
            setResult(1111, resultIntent);
            Log.d("Trying to add shifts", "sending data to "+resultIntent.toString());

            this.finish();

        } catch (ParseException e) {
            e.printStackTrace();
        }



    }

    /**
     * Exit
     * @param view
     */
    public void onBackPressed(View view) {
        this.finish();
    }

}
