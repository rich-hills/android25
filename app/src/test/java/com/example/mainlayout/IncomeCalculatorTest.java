package com.example.mainlayout;

import android.util.Log;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class IncomeCalculatorTest {

    @Test
    public void shiftPay() {
        try {
            SimpleDateFormat datesBad = new SimpleDateFormat("d:M:y");
            UserSettings user = new UserSettings("Bob",20000,
                    datesBad.parse("01:01:2000"));
            user.addJob("test",21.2);
            LinkedList<Shift> shifts = new LinkedList<Shift>();

            SimpleDateFormat f = new SimpleDateFormat("k:m");
            Date start = f.parse("10:00");
            Date end = f.parse("15:00");

            shifts.add(new Shift("test", "Mon", start, end));
            IncomeCalculator calc = new IncomeCalculator(user,shifts);
            assertEquals(106,calc.findIncomeBeforeTax(),0.05);

        } catch (ParseException e){
            Log.e("Error: ", e.getMessage());
        }
    }

    @Test
    public void shiftPayCasual() {
        try {
            SimpleDateFormat datesBad = new SimpleDateFormat("d:M:y");
            UserSettings user = new UserSettings("Bob",20000,
                    datesBad.parse("01:01:2000"));
            user.addJob("test",8);
            user.getJob("test").setCasual(true);
            LinkedList<Shift> shifts = new LinkedList<Shift>();

            SimpleDateFormat f = new SimpleDateFormat("k:m");
            Date start = f.parse("10:00");
            Date end = f.parse("15:00");

            shifts.add(new Shift("test", "Mon", start, end));
            IncomeCalculator calc = new IncomeCalculator(user,shifts);
            assertEquals(50,calc.findIncomeBeforeTax(),0.05);

        } catch (ParseException e){
            Log.e("Error: ", e.getMessage());
        }
    }

    @Test
    public void shiftPayWithPenalty(){
        try {
            SimpleDateFormat datesBad = new SimpleDateFormat("d:M:y");
            UserSettings user = new UserSettings("Bob",20000,
                    datesBad.parse("01:01:2000"));
            user.addJob("test",8);

            LinkedList<Shift> shifts = new LinkedList<Shift>();

            SimpleDateFormat f = new SimpleDateFormat("E:k:m");
            Date start = f.parse("Mon:10:00");
            Date end = f.parse("Mon:15:00");
            CustomDate pStart = new CustomDate("Mon",10,0);
            CustomDate pEnd = new CustomDate("Mon",15,0);
            user.getJob("test").addPenalty("Mon",pStart,pEnd,0.25);

            shifts.add(new Shift("test", "Mon", start, end));
            IncomeCalculator calc = new IncomeCalculator(user,shifts);
            assertEquals(50,calc.findIncomeBeforeTax(),0.05);

        } catch (ParseException e){
            Log.e("Error: ", e.getMessage());
        }
    }

    @Test
    public void shiftPayWithMultiplePenalties(){
        try {


            LinkedList<Shift> shifts = new LinkedList<Shift>();

            SimpleDateFormat f = new SimpleDateFormat("E:k:m");

            SimpleDateFormat datesBad = new SimpleDateFormat("d:M:y");
            UserSettings user = new UserSettings("Bob",20000,
                    datesBad.parse("01:01:2000"));
            user.addJob("test",8);

            Date start = f.parse("Mon:10:00");
            Date end = f.parse("Mon:15:00");
            CustomDate pStart = new CustomDate("Mon",10,0);
            CustomDate pMid = new CustomDate("Mon",12,0);
            CustomDate pEnd = new CustomDate("Mon",15,0);
            CustomDate pOut = new CustomDate("Mon",18,0);
            user.getJob("test").addPenalty("Mon",pStart,pMid,0.25);
            user.getJob("test").addPenalty("Mon",pMid,pEnd,0.25);
            user.getJob("test").addPenalty("Mon",pEnd,pOut,1);

            shifts.add(new Shift("test", "Mon", start, end));
            IncomeCalculator calc = new IncomeCalculator(user,shifts);
            assertEquals(50,calc.findIncomeBeforeTax(),0.05);

        } catch (ParseException e){
            Log.e("Error: ", e.getMessage());
        }
    }

    @Test
    public void multipleShiftsPayWithMultiplePenalties(){
        try {
            SimpleDateFormat datesBad = new SimpleDateFormat("d:M:y");
            UserSettings user = new UserSettings("Bob",20000,
                    datesBad.parse("01:01:2000"));
            user.addJob("test",8);

            LinkedList<Shift> shifts = new LinkedList<Shift>();

            SimpleDateFormat f = new SimpleDateFormat("E:k:m");
            Date start = f.parse("Mon:10:00");
            Date end = f.parse("Mon:15:00");
            Date later = f.parse("Mon:18:00");
            CustomDate pStart = new CustomDate("Mon",10,0);
            CustomDate pMid = new CustomDate("Mon",12,0);
            CustomDate pEnd = new CustomDate("Mon",15,0);
            CustomDate pOut = new CustomDate("Mon",18,0);
            user.getJob("test").addPenalty("Mon",pStart,pMid,0.25);
            user.getJob("test").addPenalty("Mon",pMid,pEnd,0.25);
            user.getJob("test").addPenalty("Mon",pEnd,pOut,1);

            shifts.add(new Shift("test", "Mon", start, end));
            shifts.add(new Shift("test","Mon", end,later));
            IncomeCalculator calc = new IncomeCalculator(user,shifts);
            assertEquals(98,calc.findIncomeBeforeTax(),0.05);

        } catch (ParseException e){
            Log.e("Error: ", e.getMessage());
        }
    }

    @Test
    public void weeksTillEofyTest(){

        SimpleDateFormat year = new SimpleDateFormat("y.M.d");
        try {
           Date first = year.parse("2020.08.1");
           Date second = year.parse("2021.04.1");

           assertEquals(47, IncomeCalculator.weeksToEOFY(first));
           assertEquals(13, IncomeCalculator.weeksToEOFY(second));


        } catch (ParseException e){

        }

    }

}
